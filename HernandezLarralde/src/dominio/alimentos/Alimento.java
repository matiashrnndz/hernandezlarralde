/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Nutriente.Nutrientes;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author matiashrnndz
 */
public class Alimento implements Serializable {

    public enum TipoAlimento {
        Fruta,
        Cereal,
        Legumbre,
        Lacteo,
        Carne,
        Huevo,
        Verdura,
        Lípido,
        Bebida
    }

    private String nombre;
    private ArrayList<TipoAlimento> tipoAlimento;
    private ArrayList<Nutriente> nutrientes;

    public Alimento() {
        this.nombre = "";
        this.tipoAlimento = new ArrayList<>();
        this.nutrientes = new ArrayList<>();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipoAlimento(ArrayList<TipoAlimento> tipoAlimento) {
        this.tipoAlimento = tipoAlimento;
    }

    public void setNutrientes(ArrayList<Nutriente> nutrientes) {
        this.nutrientes = nutrientes;
    }

    public String getNombre() {
        return nombre;
    }

    public ArrayList<TipoAlimento> getTipoAlimento() {
        return tipoAlimento;
    }

    public ArrayList<Nutriente> getNutrientes() {
        return nutrientes;
    }

    public boolean tieneTipoAlimento(ArrayList<TipoAlimento> tiposNuevos) {
        int cantTipoAlimento;
        cantTipoAlimento = tipoAlimento.size();

        int cantTiposNuevos;
        cantTiposNuevos = tiposNuevos.size();

        for (int i = 0; i < cantTipoAlimento; i++) {
            for (int j = 0; j < cantTiposNuevos; j++) {
                if (tipoAlimento.get(i) == tiposNuevos.get(j)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean tieneNutrientes(ArrayList<Nutrientes> restringidos) {
        int cantNutrientes;
        cantNutrientes = nutrientes.size();

        int cantRestringidos;
        cantRestringidos = restringidos.size();

        for (int i = 0; i < cantNutrientes; i++) {
            for (int j = 0; j < cantRestringidos; j++) {
                if (nutrientes.get(i).getNombre() == restringidos.get(j)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
