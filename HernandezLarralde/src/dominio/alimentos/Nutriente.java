/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Nutriente.Unidades;
import static dominio.alimentos.Nutriente.Nutrientes.Energía;
import static dominio.alimentos.Nutriente.Unidades.kcal;
import java.io.Serializable;

/**
 *
 * @author matiashrnndz
 */
public class Nutriente implements Serializable {

    public enum Nutrientes {
        Energía,
        Proteína,
        Grasa,
        Fibra,
        Sodio,
        Hierro,
        Azúcares,
        Calcio
    }

    public enum Unidades {
        kcal, //KILOCALORIA
        g, //GRAMOS
        mg, //MILIGRAMOS
    }

    private Nutrientes nombre;
    private int valor;
    private Unidades unidad;

    public Nutriente() {
        this.nombre = Energía;
        this.valor = 0;
        this.unidad = kcal;
    }

    public void setNombre(Nutrientes nombre) {
        this.nombre = nombre;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public void setUnidad(Unidades unidad) {
        this.unidad = unidad;
    }
//
//    @Override
//    public String toString() {
//        String unidadNutriente = unidadesAString();
//        String nombreNutriente = nombreAString();
//
//        return nombreNutriente + " " + valor + " " + unidadNutriente + ".";
//    }
//
//    private String unidadesAString() {
//        String unidadString = unidad.name();
//        unidadString = unidadString.toLowerCase();
//        //unidadString = Character.toUpperCase(unidadString.charAt(0))
//        //        + unidadString.substring(1);
//
//        return unidadString;
//    }
//
//    private String nombreAString() {
//        String nombreString = nombre.name();
//        nombreString = nombreString.toLowerCase();
//        nombreString = Character.toUpperCase(nombreString.charAt(0))
//                + nombreString.substring(1);
//
//        return nombreString;
//    }

    public Nutrientes getNombre() {
        return nombre;
    }

}
