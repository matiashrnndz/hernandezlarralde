/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.personas.Profesional;
import java.io.Serializable;

/**
 *
 * @author matiashrnndz
 */
public class PlanAlimentacionSemanal implements Serializable {

    private final Profesional profesional;
    private final Alimento[][] planAlimentacionSemanal;

    public PlanAlimentacionSemanal(Profesional profesional) {
        this.profesional = profesional;
        planAlimentacionSemanal = new Alimento[4][7];
    }

    public void agregarAlimento(int indexComida, int indexDia, Alimento alimento) {
        try {
            planAlimentacionSemanal[indexComida][indexDia] = alimento;
        } catch (ArrayIndexOutOfBoundsException e) {
            assert (false);
        }
    }

    public String getNombreAlimentoPorPosicion(int indexComida, int indexDia) {
        try {
            return planAlimentacionSemanal[indexComida][indexDia].toString();
        } catch (ArrayIndexOutOfBoundsException e) {
            return "";
        }
    }

    public Profesional getProfesional() {
        return profesional;
    }

}
