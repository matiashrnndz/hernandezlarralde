/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Alimento.TipoAlimento;
import dominio.repositorio.Repositorio;
import java.util.ArrayList;

/**
 *
 * @author matiashrnndz
 */
public class RegistroAlimento {

    private final Alimento alimento;

    public RegistroAlimento() {
        alimento = new Alimento();
    }

    public void registrarNombre(String nombre) {
        alimento.setNombre(nombre);
    }

    public void registrarTiposAlimento(ArrayList<TipoAlimento> tiposAlimento) {
        alimento.setTipoAlimento(tiposAlimento);
    }

    public void registrarNutrientes(ArrayList<Nutriente> nutrientes) {
        alimento.setNutrientes(nutrientes);
    }

    public void registrarAlimentoEnRepositorio(Repositorio repositorio) {
        repositorio.agregarAlimento(alimento);
    }
}
