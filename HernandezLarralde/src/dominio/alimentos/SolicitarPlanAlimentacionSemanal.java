/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.personas.Profesional;
import dominio.personas.Usuario;
import dominio.repositorio.Repositorio;
import dominio.validadores.ValidadorPlanAlimentacion;

/**
 *
 * @author matiashrnndz
 */
public class SolicitarPlanAlimentacionSemanal {

    private static final int CANT_DIAS_SEMANA = 7;
    private static final int CANT_COMIDAS = 4;

    private final PlanAlimentacionSemanal plan;
    private final Usuario usuario;
    private final ValidadorPlanAlimentacion validador;
    private final Repositorio repositorio;

    public SolicitarPlanAlimentacionSemanal(Usuario usuario, Profesional profesional, Repositorio repositorio) {
        this.repositorio = repositorio;
        this.usuario = usuario;
        this.plan = new PlanAlimentacionSemanal(profesional);
        validador = new ValidadorPlanAlimentacion(usuario, repositorio);
    }

    public void crearPlan() {
        Alimento alimento;
        for (int f = 0; f < CANT_COMIDAS; f++) {
            for (int c = 0; c < CANT_DIAS_SEMANA; c++) {
                do {
                    alimento = repositorio.getAlimentoAlAzar();
                } while (!validador.validarAlimento(alimento));
                
                plan.agregarAlimento(f, c, alimento);
            }
        }
    }

    public void registrarPlanAUsuario() {
        if (usuario.existePlanAlimentacionSemanal(plan)) {
            usuario.actualizarPlanAlimentacionSemanal(plan);
        } else {
            usuario.agregarPlanAlimentacionSemanal(plan);
        }
    }
    
}
