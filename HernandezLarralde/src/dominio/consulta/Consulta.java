/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.consulta;

import dominio.personas.Profesional;
import java.io.Serializable;

/**
 *
 * @author matiashrnndz
 */
public class Consulta implements Serializable {

    private Profesional profesional;
    private String historial;
    private String consulta;

    public Consulta() {
        historial = "";
    }

    public Consulta(Profesional profesionalAConsultar) {
        historial = "";
        this.profesional = profesionalAConsultar;
    }

    public void consultar(String consulta) {
        this.consulta = consulta;
        historial = historial + "Usuario : " + consulta + "\n \n";
    }

    public void responder(String respuesta) {
        historial = historial + "Profesional : " + respuesta + "\n \n";
    }

    public String getConsulta() {
        return consulta;
    }

    public Profesional getProfesional() {
        return profesional;
    }

    public String getHistorial() {
        return historial;
    }

}
