/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import java.util.Date;
import java.awt.image.BufferedImage;
import java.io.Serializable;

/**
 *
 * @author matiashrnndz
 */
public abstract class Persona implements Serializable {

    protected String nombre;
    protected String apellido;
    protected Date fechaNacimiento;
    protected BufferedImage fotoPerfil;

    protected Persona() {
        this.nombre = "";
        this.apellido = "";
        this.fotoPerfil = null; //cambiar los null por un default
        this.fechaNacimiento = null; //cambiar los null por un default
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setFotoPerfil(BufferedImage fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public BufferedImage getFotoPerfil() {
        return fotoPerfil;
    }

}
