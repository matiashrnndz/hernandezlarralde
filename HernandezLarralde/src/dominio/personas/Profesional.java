/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import java.util.Date;

/**
 *
 * @author matiashrnndz
 */
public class Profesional extends Persona {

    private int id;
    private String titulo;
    private Date fechaTitulo;
    private String pais;

    public Profesional() {
        super();
        this.titulo = "";
        this.pais = "";
        this.fechaTitulo = null;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public Date getFechaTitulo() {
        return fechaTitulo;
    }

    public String getPais() {
        return pais;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString()
    {
        return nombre + " " + apellido; 
    }
}
