/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import dominio.repositorio.Repositorio;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class RegistroProfesional {

    private final Profesional profesional;

    public RegistroProfesional() {
        profesional = new Profesional();
    }

    public Profesional getProfesional() {
        return profesional;
    }

    public void registrarNombre(String nombre) {
        profesional.setNombre(nombre);
    }

    public void registrarApellido(String apellido) {
        profesional.setApellido(apellido);
    }

    public void registrarNombreTitulo(String nombreTitulo) {
        profesional.setTitulo(nombreTitulo);
    }

    public void registrarFechaTitulo(Date fechaTitulo) {
        profesional.setFechaTitulo(fechaTitulo);
    }

    public void registrarPaisTitulo(String pais) {
        profesional.setPais(pais);
    }

    public void registrarNacimiento(Date fechaNacimiento) {
        profesional.setFechaNacimiento(fechaNacimiento);
    }

    public void registrarProfesionalEnRepositorio(Repositorio repositorio) {
        repositorio.agregarProfesional(profesional);
    }

}
