/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import dominio.repositorio.Repositorio;
import dominio.repositorio.Repositorio.PreferenciasAlimenticias;
import dominio.repositorio.Repositorio.RestriccionesAlimenticias;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author matiashrnndz
 */
public class RegistroUsuario {

    private final Usuario usuario;

    public RegistroUsuario() {
        this.usuario = new Usuario();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void registrarNombre(String nombre) {
        usuario.setNombre(nombre);
    }

    public void registrarApellido(String apellido) {
        usuario.setApellido(apellido);
    }

    public void registrarNacionalidad(String nacionalidad) {
        usuario.setNacionalidad(nacionalidad);
    }

    public void registrarPreferencias(ArrayList<PreferenciasAlimenticias> preferencias) {
        usuario.setPreferenciasAlimenticias(preferencias);
    }

    public void registrarRestricciones(ArrayList<RestriccionesAlimenticias> restricciones) {
        usuario.setRestriccionesAlimenticias(restricciones);
    }

    public void registrarNacimiento(Date nacimiento) {
        usuario.setFechaNacimiento(nacimiento);
    }

    public void registrarUsuarioEnRepositorio(Repositorio repositorio) {
        repositorio.agregarUsuario(usuario);
    }

}
