/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import dominio.alimentos.Alimento;
import dominio.alimentos.PlanAlimentacionSemanal;
import dominio.consulta.Consulta;
import dominio.repositorio.Repositorio.PreferenciasAlimenticias;
import dominio.repositorio.Repositorio.RestriccionesAlimenticias;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author matiashrnndz
 */
public class Usuario extends Persona {

    private String nacionalidad;
    private final ArrayList<Consulta> consultas;
    private ArrayList<PreferenciasAlimenticias> preferencias;
    private ArrayList<RestriccionesAlimenticias> restricciones;
    private final ArrayList<PlanAlimentacionSemanal> planAlimentacionSemanal;
    private final Hashtable<Date, ArrayList<Alimento>> ingestasDiarias;

    public Usuario() {
        super();

        ingestasDiarias = new Hashtable<>();
        preferencias = new ArrayList<>();
        restricciones = new ArrayList<>();
        consultas = new ArrayList<>();
        planAlimentacionSemanal = new ArrayList<>();
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public void setPreferenciasAlimenticias(ArrayList<PreferenciasAlimenticias> preferencias) {
        this.preferencias = preferencias;
    }

    public void setRestriccionesAlimenticias(ArrayList<RestriccionesAlimenticias> restricciones) {
        this.restricciones = restricciones;
    }

    public final String getNacionalidad() {
        return nacionalidad;
    }

    public ArrayList<PreferenciasAlimenticias> getPreferencias() {
        return preferencias;
    }

    public ArrayList<RestriccionesAlimenticias> getRestricciones() {
        return restricciones;
    }

    public ArrayList<Consulta> getConsultas() {
        return consultas;
    }

    public ArrayList<PlanAlimentacionSemanal> getPlanAlimentacionSemanal() {
        return planAlimentacionSemanal;
    }

    public Consulta getConsultaDe(Profesional profesional) {
        Consulta consulta = new Consulta();

        for (int i = 0; i < consultas.size(); i++) {
            Consulta consultaActual = consultas.get(i);
            if (consultaActual.getProfesional().getId() == profesional.getId()) {
                return consultaActual;
            }
        }

        assert (false);
        return consulta;
    }

    public PlanAlimentacionSemanal getPlanAlimentacionSemanalDe(Profesional profesional) {
        int id = profesional.getId();

        for (int i = 0; i < planAlimentacionSemanal.size(); i++) {

            int idActual;
            idActual = planAlimentacionSemanal.get(i).getProfesional().getId();
            if (idActual == id) {
                return planAlimentacionSemanal.get(i);
            }
        }

        assert (false);
        return new PlanAlimentacionSemanal(profesional);
    }

    public Hashtable<Date, ArrayList<Alimento>> getIngestasDiarias() {
        return ingestasDiarias;
    }

    public void agregarConsulta(Consulta consulta) {
        consultas.add(consulta);
    }

    public void agregarIngestaDeHoy(ArrayList<Alimento> alimentos) {
        Date hoy = Calendar.getInstance().getTime();
        hoy = truncarFecha(hoy);

        ingestasDiarias.put(hoy, alimentos);
    }

    public void agregarPlanAlimentacionSemanal(PlanAlimentacionSemanal plan) {
        this.planAlimentacionSemanal.add(plan);
    }

    public void actualizarIngestaDeHoy(ArrayList<Alimento> ingestasNuevas) {
        Date hoy = Calendar.getInstance().getTime();
        hoy = truncarFecha(hoy);

        ingestasDiarias.replace(hoy, ingestasNuevas);
    }

    public void actualizarPlanAlimentacionSemanal(PlanAlimentacionSemanal plan) {
        int id = plan.getProfesional().getId();

        for (int i = planAlimentacionSemanal.size() - 1; i >= 0; i--) {

            int idActual;
            idActual = planAlimentacionSemanal.get(i).getProfesional().getId();
            if (idActual == id) {
                planAlimentacionSemanal.remove(i);
            }
        }

        planAlimentacionSemanal.add(plan);
    }

    public boolean existeConsulta(Profesional profesional) {
        int idProfesional = profesional.getId();

        for (int i = 0; i < consultas.size(); i++) {
            Consulta consultaActual = consultas.get(i);

            if (consultaActual.getProfesional().getId() == idProfesional) {
                return true;
            }
        }

        return false;
    }

    public boolean existeIngestaDeHoy() {
        Date hoy = Calendar.getInstance().getTime();
        hoy = truncarFecha(hoy);

        return ingestasDiarias.containsKey(hoy);
    }

    public boolean existePlanAlimentacionSemanal(PlanAlimentacionSemanal plan) {
        int id = plan.getProfesional().getId();

        for (int i = 0; i < planAlimentacionSemanal.size(); i++) {

            int idActual;
            idActual = planAlimentacionSemanal.get(i).getProfesional().getId();
            if (idActual == id) {
                return true;
            }
        }

        return false;
    }

    private Date truncarFecha(Date fecha) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        fecha = calendar.getTime();

        return fecha;
    }

}
