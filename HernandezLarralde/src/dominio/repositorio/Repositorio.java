/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.repositorio;

import dominio.alimentos.Alimento;
import dominio.consulta.Consulta;
import dominio.personas.Profesional;
import dominio.personas.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author matiashrnndz
 */
public class Repositorio implements Serializable {

    private int idProfesional;
    private final ArrayList<Profesional> profesionales;
    private final ArrayList<Usuario> usuarios;
    private final ArrayList<Alimento> alimentos;

    public enum PreferenciasAlimenticias {
        Vegetariano,
        Vegano,
        Ovolacteovegetariano
    }

    public enum RestriccionesAlimenticias {
        Diabético,
        Hipertenso
    }

    public Repositorio() {
        this.profesionales = new ArrayList<>();
        this.usuarios = new ArrayList<>();
        this.alimentos = new ArrayList<>();
        idProfesional = -1;
    }

    public Alimento getAlimentoAlAzar() {
        int cantAlimentos = alimentos.size();

        Alimento alimento;

        if (cantAlimentos > 0) {
            Random random = new Random();

            int menor = 0;
            int mayor = alimentos.size();
            int indexAlAzar = random.nextInt(mayor - menor) + menor;

            alimento = alimentos.get(indexAlAzar);
        } else {
            alimento = new Alimento();
        }

        return alimento;
    }

    public int getIdProfesional() {
        idProfesional++;
        return idProfesional;
    }

    public ArrayList<Profesional> getProfesionales() {
        return profesionales;
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public ArrayList<Alimento> getAlimentos() {
        return alimentos;
    }

    public ArrayList<Usuario> getUsuariosConConsulta(Profesional profesionalAConsultar) {
        ArrayList<Usuario> usuariosConConsulta = new ArrayList<>();
        int idProfesionalAConsultar = profesionalAConsultar.getId();

        for (int i = 0; i < usuarios.size(); i++) {
            Usuario usuarioActual = usuarios.get(i);
            ArrayList<Consulta> consultasUsuario = usuarioActual.getConsultas();
            if (!consultasUsuario.isEmpty()) {
                for (int j = 0; j < consultasUsuario.size(); j++) {
                    Consulta consultaActual = consultasUsuario.get(j);
                    int idProfesionalActual = consultaActual.getProfesional().getId();
                    if (idProfesionalActual == idProfesionalAConsultar) {
                        usuariosConConsulta.add(usuarioActual);
                    }
                }
            }
        }

        return usuariosConConsulta;
    }

    public void agregarProfesional(Profesional profesional) {
        if (profesional != null) {
            profesional.setId(getIdProfesional());
            profesionales.add(profesional);
        }
    }

    public void agregarUsuario(Usuario usuario) {
        if (usuario != null) {
            usuarios.add(usuario);
        }
    }

    public void agregarAlimento(Alimento alimento) {
        if (alimento != null) {
            alimentos.add(alimento);
        }
    }

}
