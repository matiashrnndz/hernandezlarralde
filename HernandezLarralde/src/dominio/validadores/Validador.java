/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

/**
 *
 * @author matiashrnndz
 */
public abstract class Validador {

    public boolean validarNombre(String nombre) {
        return !nombre.matches(".*\\d+.*");
    }

    public boolean hayNombre(String nombre) {
        return !(nombre.equalsIgnoreCase("") || nombre.equalsIgnoreCase("Nombre"));
    }

}
