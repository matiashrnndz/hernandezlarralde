/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorPersona extends Validador {
    
    public boolean validarApellido(String apellido) {
        return !apellido.matches(".*\\d+.*");
    }

    public boolean validarNacionalidad(String nacionalidad) {
        return !nacionalidad.matches(".*\\d+.*");
    }

    public boolean hayApellido(String apellido) {
        return !(apellido.equalsIgnoreCase("") || apellido.equalsIgnoreCase("Apellido"));
    }

    public boolean hayNacionalidad(String nacionalidad) {
        return !(nacionalidad.equalsIgnoreCase("") || nacionalidad.equalsIgnoreCase("Nacionalidad"));
    }
    
}
