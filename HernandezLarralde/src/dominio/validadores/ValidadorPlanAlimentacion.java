/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import dominio.alimentos.Alimento;
import dominio.alimentos.Alimento.TipoAlimento;
import dominio.alimentos.Nutriente.Nutrientes;
import dominio.personas.Usuario;
import dominio.repositorio.Repositorio;
import dominio.repositorio.Repositorio.PreferenciasAlimenticias;
import dominio.repositorio.Repositorio.RestriccionesAlimenticias;
import java.util.ArrayList;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorPlanAlimentacion {

    private final Usuario usuario;
    private final Repositorio repositorio;

    public ValidadorPlanAlimentacion(Usuario usuario, Repositorio repositorio) {
        this.usuario = usuario;
        this.repositorio = repositorio;
    }

    public boolean existeAlgunAlimentoValido() {
        int cantAlimentos = repositorio.getAlimentos().size();

        if (cantAlimentos > 0) {
            ArrayList<Alimento> alimentos = repositorio.getAlimentos();
            for (int i = 0; i < cantAlimentos; i++) {
                Alimento alimento = alimentos.get(i);
                if (validarAlimento(alimento)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean validarAlimento(Alimento alimento) {
        return respetaPreferencias(alimento) && respetaRestricciones(alimento);
    }

    private boolean respetaPreferencias(Alimento alimento) {
        ArrayList<PreferenciasAlimenticias> preferencias;
        preferencias = usuario.getPreferencias();

        ArrayList<TipoAlimento> restringidos;
        restringidos = preferenciasATipoAlimentos(preferencias);

        return !(alimento.tieneTipoAlimento(restringidos));
    }

    private boolean respetaRestricciones(Alimento alimento) {
        ArrayList<RestriccionesAlimenticias> restricciones;
        restricciones = usuario.getRestricciones();

        ArrayList<Nutrientes> restringidos;
        restringidos = restriccionesANutrientes(restricciones);

        return !(alimento.tieneNutrientes(restringidos));
    }

    private ArrayList<TipoAlimento> preferenciasATipoAlimentos(ArrayList<PreferenciasAlimenticias> preferencias) {
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();

        PreferenciasAlimenticias preferencia;
        for (int i = 0; i < preferencias.size(); i++) {
            preferencia = preferencias.get(i);
            ArrayList<TipoAlimento> tiposAlimentoActual;
            tiposAlimentoActual = preferenciaATipoAlimento(preferencia);

            for (int j = 0; j < tiposAlimentoActual.size(); j++) {
                tiposAlimento.add(tiposAlimentoActual.get(j));
            }
        }

        return tiposAlimento;
    }

    private ArrayList<Nutrientes> restriccionesANutrientes(ArrayList<RestriccionesAlimenticias> restricciones) {
        ArrayList<Nutrientes> nutrientesAlimento = new ArrayList<>();

        RestriccionesAlimenticias restriccion;
        for (int i = 0; i < restricciones.size(); i++) {
            restriccion = restricciones.get(i);
            ArrayList<Nutrientes> nutrientesAlimentoActual;
            nutrientesAlimentoActual = restriccionANutriente(restriccion);

            for (int j = 0; j < nutrientesAlimentoActual.size(); j++) {
                nutrientesAlimento.add(nutrientesAlimentoActual.get(j));
            }
        }

        return nutrientesAlimento;
    }
    
    private ArrayList<TipoAlimento> preferenciaATipoAlimento(PreferenciasAlimenticias preferencia) {
        ArrayList<TipoAlimento> tiposAlimento;
        tiposAlimento = new ArrayList<>();

        switch (preferencia) {
            case Vegetariano:
                tiposAlimento.add(TipoAlimento.Carne);
                break;

            case Vegano:
                tiposAlimento.add(TipoAlimento.Carne);
                tiposAlimento.add(TipoAlimento.Huevo);
                break;

            case Ovolacteovegetariano:
                tiposAlimento.add(TipoAlimento.Carne);
                tiposAlimento.add(TipoAlimento.Huevo);
                tiposAlimento.add(TipoAlimento.Lacteo);
                break;

            default:
                assert (false);
                break;
        }

        return tiposAlimento;
    }
    
    private ArrayList<Nutrientes> restriccionANutriente(RestriccionesAlimenticias restriccion) {
        ArrayList<Nutrientes> nutrientesAlimento;
        nutrientesAlimento = new ArrayList<>();

        switch (restriccion) {
            case Diabético:
                nutrientesAlimento.add(Nutrientes.Azúcares);
                break;

            case Hipertenso:
                nutrientesAlimento.add(Nutrientes.Sodio);
                break;

            default :
                assert (false);
                break;
        }

        return nutrientesAlimento;
    }
}
