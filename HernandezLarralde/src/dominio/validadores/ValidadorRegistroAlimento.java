/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorRegistroAlimento extends Validador {

    public boolean validarValorNutriente(int valorActual) {
        return valorActual >= 0;
    }

    public boolean validarPertenenciaTipoAlimento(boolean pertenece) {
        return pertenece;
    }
    
}
