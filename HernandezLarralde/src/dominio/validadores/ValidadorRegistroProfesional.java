/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

/**
 *
 * @author Usuario
 */
public class ValidadorRegistroProfesional extends ValidadorPersona {

    public boolean validarPaisTitulo(String paisTitulo) {
        return !paisTitulo.matches(".*\\d+.*");
    }

    public boolean hayPaisTitulo(String paisTitulo) {
        return !(paisTitulo.equalsIgnoreCase("") || paisTitulo.equalsIgnoreCase("País del título :"));
    }

    public boolean hayTitulo(String titulo) {
        return !(titulo.equalsIgnoreCase("") || titulo.equalsIgnoreCase("Título"));
    }

    public boolean validarTitulo(String titulo) {
        return !titulo.matches(".*\\d+.*");
    }

}
