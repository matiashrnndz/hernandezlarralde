/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hernandezlarralde;

import dominio.repositorio.Repositorio;
import interfaz_usuario.FramePrincipal;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author Matias Hernández (169234), Santiago Larralde (217922)
 */
public class HernandezLarralde {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Repositorio repositorio = new Repositorio();

        File f = new File("Repositorio.txt");

        try {
            FileInputStream fis = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fis);
            repositorio = (Repositorio) ois.readObject();
        } catch (ClassNotFoundException | IOException e) {

        }
//
//        Profesional profesional = new Profesional();
//        profesional.setNombre("Ángel");
//        profesional.setApellido("Aran");
//        profesional.setTitulo("Nutricionista");
//        profesional.setPais("Argentina");
//        repositorio.agregarProfesional(profesional);
//
//        Profesional profesional2 = new Profesional();
//        profesional2.setNombre("Brenda");
//        profesional2.setApellido("Britos");
//        profesional2.setTitulo("Nutricionista");
//        profesional2.setPais("Brasil");
//        repositorio.agregarProfesional(profesional2);
//
//        Usuario usuario1 = new Usuario();
//        usuario1.setNombre("Creo");
//        usuario1.setApellido("Creito");
//        usuario1.setNacionalidad("Croacia");
//        repositorio.agregarUsuario(usuario1);
//
//        Alimento alimento = new Alimento();
//        alimento.setNombre("Alimento 1");
//        repositorio.agregarAlimento(alimento);
//
//        Alimento alimento2 = new Alimento();
//        alimento2.setNombre("Alimento 2");
//        repositorio.agregarAlimento(alimento2);
//
//        Alimento alimento3 = new Alimento();
//        alimento3.setNombre("Alimento 3");
//        repositorio.agregarAlimento(alimento3);

//
//        Profesional profesional = new Profesional();
//        profesional.setNombre("Ángel");
//        profesional.setApellido("Aran");
//        profesional.setTitulo("Nutricionista");
//        profesional.setPais("Argentina");
//        repositorio.agregarProfesional(profesional);
//
//        Profesional profesional2 = new Profesional();
//        profesional2.setNombre("Brenda");
//        profesional2.setApellido("Britos");
//        profesional2.setTitulo("Nutricionista");
//        profesional2.setPais("Brasil");
//        repositorio.agregarProfesional(profesional2);
//
//        Usuario usuario1 = new Usuario();
//        usuario1.setNombre("Creo");
//        usuario1.setApellido("Creito");
//        usuario1.setNacionalidad("Croacia");
//        repositorio.agregarUsuario(usuario1);
//
//        Alimento alimento = new Alimento();
//        alimento.setNombre("Alimento 1");
//        repositorio.agregarAlimento(alimento);
//
//        Alimento alimento2 = new Alimento();
//        alimento2.setNombre("Alimento 2");
//        repositorio.agregarAlimento(alimento2);
//
//        Alimento alimento3 = new Alimento();
//        alimento3.setNombre("Alimento 3");
//        repositorio.agregarAlimento(alimento3);
//        Consulta consulta = new Consulta(repositorio.getProfesionales().get(0));
//        consulta.consultar("Hola, me gustaría saber las ventajas y desventajas que tiene comer manzana todas las mañanas.");
//        repositorio.getUsuarios().get(0).agregarConsulta(consulta);
        FramePrincipal ventanaPrincipal = new FramePrincipal(repositorio);
        ventanaPrincipal.setVisible(true);
    }

}
