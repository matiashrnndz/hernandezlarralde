/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz_usuario;

import dominio.personas.Persona;
import dominio.personas.Profesional;
import dominio.personas.Usuario;
import dominio.repositorio.Repositorio;
import interfaz_usuario.panel_principal.PanelLogIn;
import interfaz_usuario.panel_principal.PanelPlanAlimentacion;
import interfaz_usuario.panel_principal.PanelRegistrarUsuario;
import interfaz_usuario.panel_principal.PanelRegistrarProfesional;
import interfaz_usuario.barra_lateral.BarraLateralLogIn;
import interfaz_usuario.barra_lateral.BarraLateralProfesional;
import interfaz_usuario.barra_lateral.BarraLateralUsuario;
import interfaz_usuario.panel_principal.PanelConsultaAProfesional;
import interfaz_usuario.panel_principal.PanelRegistrarAlimento;
import interfaz_usuario.panel_principal.PanelRegistrarIngesta;
import interfaz_usuario.panel_principal.PanelResponderConsulta;
import interfaz_usuario.panel_superior.PanelSuperior;
import java.awt.CardLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JRootPane;

/**
 *
 * @author matiashrnndz
 */
public final class FramePrincipal extends javax.swing.JFrame {

    Repositorio repositorio;
    Persona persona;

    /**
     * Creates new form Principal
     *
     * @param repositorio
     */
    public FramePrincipal(Repositorio repositorio) {
        quitarBarraVentanaArriba();

        initComponents();

        setLocationRelativeTo(null);

        this.repositorio = repositorio;

        mostrarBarraSuperior();
        mostrarBarraLateralLogIn();
        mostrarLogIn();
    }

    public Repositorio getRepositorio() {
        return repositorio;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void mostrarBarraLateralLogIn() {
        BarraLateralLogIn barraLateralDeLogIn = new BarraLateralLogIn(this);

        barraLateral.removeAll();
        barraLateral.setLayout(new CardLayout(BoxLayout.X_AXIS, BoxLayout.Y_AXIS));
        barraLateral.add(barraLateralDeLogIn);
        revalidate();
    }

    public void mostrarBarraLateralUsuario() {
        BarraLateralUsuario barraLateralDeUsuario = new BarraLateralUsuario(this);

        barraLateral.removeAll();
        barraLateral.setLayout(new CardLayout(BoxLayout.X_AXIS, BoxLayout.Y_AXIS));
        barraLateral.add(barraLateralDeUsuario);
        revalidate();
    }

    public void mostrarBarraLateralProfesional() {
        BarraLateralProfesional barraLateralDeProfesional = new BarraLateralProfesional(this);

        barraLateral.removeAll();
        barraLateral.setLayout(new CardLayout(BoxLayout.X_AXIS, BoxLayout.Y_AXIS));
        barraLateral.add(barraLateralDeProfesional);
        revalidate();
    }

    private void mostrarBarraSuperior() {
        PanelSuperior panelSuperior = new PanelSuperior(this);

        barraSuperior.removeAll();
        barraSuperior.setLayout(new CardLayout(BoxLayout.X_AXIS, BoxLayout.Y_AXIS));
        barraSuperior.add(panelSuperior);
        revalidate();
    }

    public void cambiarPanelPrincipal(JPanel panelNuevo) {
        panelPrincipal.removeAll();
        panelPrincipal.setLayout(new CardLayout(BoxLayout.X_AXIS, BoxLayout.Y_AXIS));
        panelPrincipal.add(panelNuevo);
        revalidate();
    }

    public void mostrarPlanAlimentacion() {
        PanelPlanAlimentacion vistaPlanAlimentacion = new PanelPlanAlimentacion(this);
        cambiarPanelPrincipal(vistaPlanAlimentacion);
    }

    public void mostrarLogIn() {
        PanelLogIn logIn = new PanelLogIn(this);
        cambiarPanelPrincipal(logIn);
    }

    public void mostrarRegistrarUsuario() {
        PanelRegistrarUsuario registrarUsuario = new PanelRegistrarUsuario(this);
        cambiarPanelPrincipal(registrarUsuario);
    }

    public void mostrarRegistrarProfesional() {
        PanelRegistrarProfesional registrarProfesional = new PanelRegistrarProfesional(this);
        cambiarPanelPrincipal(registrarProfesional);
    }

    public void mostrarRegistrarAlimento() {
        PanelRegistrarAlimento registrarAlimento = new PanelRegistrarAlimento(this);
        cambiarPanelPrincipal(registrarAlimento);
    }

    public void mostrarRegistrarIngesta() {
        PanelRegistrarIngesta registrarIngesta = new PanelRegistrarIngesta(this);
        cambiarPanelPrincipal(registrarIngesta);
    }

    public void mostrarConsultaAProfesional() {
        PanelConsultaAProfesional consultaAProfesional = new PanelConsultaAProfesional(this);
        cambiarPanelPrincipal(consultaAProfesional);
    }

    public void mostrarResponderConsulta() {
        PanelResponderConsulta responderConsulta = new PanelResponderConsulta(this);
        cambiarPanelPrincipal(responderConsulta);
    }

    public void mostrarHome() {
        if (persona instanceof Usuario) {
            mostrarHomeUsuario();
        } else if (persona instanceof Profesional) {
            mostrarHomeProfesional();
        }
        else {
            mostrarLogIn();
        }
    }

    private void mostrarHomeUsuario() {
        mostrarPlanAlimentacion();
    }

    private void mostrarHomeProfesional() {
        mostrarResponderConsulta();
    }

    private void quitarBarraVentanaArriba() {
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
    }
    
    
    public void vaciarPersona() {
        persona = null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barraLateral = new javax.swing.JPanel();
        panelPrincipal = new javax.swing.JPanel();
        barraSuperior = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(716, 654));
        setResizable(false);
        setSize(new java.awt.Dimension(716, 654));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        barraLateral.setBackground(new java.awt.Color(56, 49, 55));
        barraLateral.setMaximumSize(new java.awt.Dimension(102, 544));
        barraLateral.setMinimumSize(new java.awt.Dimension(102, 544));
        barraLateral.setName("barraLateral"); // NOI18N
        barraLateral.setPreferredSize(new java.awt.Dimension(102, 544));

        javax.swing.GroupLayout barraLateralLayout = new javax.swing.GroupLayout(barraLateral);
        barraLateral.setLayout(barraLateralLayout);
        barraLateralLayout.setHorizontalGroup(
            barraLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 102, Short.MAX_VALUE)
        );
        barraLateralLayout.setVerticalGroup(
            barraLateralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        panelPrincipal.setBackground(new java.awt.Color(56, 49, 55));
        panelPrincipal.setForeground(new java.awt.Color(51, 51, 51));
        panelPrincipal.setMaximumSize(new java.awt.Dimension(562, 544));
        panelPrincipal.setMinimumSize(new java.awt.Dimension(562, 544));
        panelPrincipal.setPreferredSize(new java.awt.Dimension(562, 544));

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 614, Short.MAX_VALUE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 578, Short.MAX_VALUE)
        );

        barraSuperior.setBackground(new java.awt.Color(56, 49, 55));
        barraSuperior.setForeground(new java.awt.Color(51, 51, 51));
        barraSuperior.setMaximumSize(new java.awt.Dimension(716, 60));
        barraSuperior.setMinimumSize(new java.awt.Dimension(716, 60));
        barraSuperior.setPreferredSize(new java.awt.Dimension(716, 60));

        javax.swing.GroupLayout barraSuperiorLayout = new javax.swing.GroupLayout(barraSuperior);
        barraSuperior.setLayout(barraSuperiorLayout);
        barraSuperiorLayout.setHorizontalGroup(
            barraSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        barraSuperiorLayout.setVerticalGroup(
            barraSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 60, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(barraSuperior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(barraLateral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(barraSuperior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
                    .addComponent(barraLateral, javax.swing.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        File f = new File("Repositorio.txt");
        try {
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(repositorio);
        } catch (IOException e) {

        }
    }//GEN-LAST:event_formWindowClosed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel barraLateral;
    private javax.swing.JPanel barraSuperior;
    private javax.swing.JPanel panelPrincipal;
    // End of variables declaration//GEN-END:variables

}
