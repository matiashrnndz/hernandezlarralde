/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz_usuario.barra_lateral;

import interfaz_usuario.FramePrincipal;

/**
 *
 * @author matiashrnndz
 */
public class BarraLateralProfesional extends javax.swing.JPanel {

    private final FramePrincipal framePrincipal;
    /**
     * Creates new form BarraLateralDeProfesional
     * @param framePrincipal
     */
    public BarraLateralProfesional(FramePrincipal framePrincipal) {
        initComponents();
        this.framePrincipal = framePrincipal;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        boton_RegistrarAlimento = new javax.swing.JButton();
        boton_ResponderConsulta = new javax.swing.JButton();
        botonSalir = new javax.swing.JButton();

        setBackground(new java.awt.Color(56, 49, 55));
        setMaximumSize(new java.awt.Dimension(102, 544));
        setMinimumSize(new java.awt.Dimension(102, 544));
        setPreferredSize(new java.awt.Dimension(102, 544));

        boton_RegistrarAlimento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz_usuario/imagenes/agregarAlimento80pxN.png"))); // NOI18N
        boton_RegistrarAlimento.setToolTipText("Registrar alimento.");
        boton_RegistrarAlimento.setBorder(null);
        boton_RegistrarAlimento.setBorderPainted(false);
        boton_RegistrarAlimento.setContentAreaFilled(false);
        boton_RegistrarAlimento.setFocusPainted(false);
        boton_RegistrarAlimento.setMaximumSize(new java.awt.Dimension(90, 90));
        boton_RegistrarAlimento.setMinimumSize(new java.awt.Dimension(90, 90));
        boton_RegistrarAlimento.setPreferredSize(new java.awt.Dimension(90, 90));
        boton_RegistrarAlimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_RegistrarAlimentoActionPerformed(evt);
            }
        });

        boton_ResponderConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz_usuario/imagenes/chat80pxN.png"))); // NOI18N
        boton_ResponderConsulta.setToolTipText("Contestar consultas.");
        boton_ResponderConsulta.setBorder(null);
        boton_ResponderConsulta.setBorderPainted(false);
        boton_ResponderConsulta.setContentAreaFilled(false);
        boton_ResponderConsulta.setFocusPainted(false);
        boton_ResponderConsulta.setMaximumSize(new java.awt.Dimension(90, 90));
        boton_ResponderConsulta.setMinimumSize(new java.awt.Dimension(90, 90));
        boton_ResponderConsulta.setPreferredSize(new java.awt.Dimension(90, 90));
        boton_ResponderConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_ResponderConsultaActionPerformed(evt);
            }
        });

        botonSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz_usuario/imagenes/logout40pxN.png"))); // NOI18N
        botonSalir.setBorder(null);
        botonSalir.setBorderPainted(false);
        botonSalir.setContentAreaFilled(false);
        botonSalir.setFocusPainted(false);
        botonSalir.setMaximumSize(new java.awt.Dimension(90, 90));
        botonSalir.setMinimumSize(new java.awt.Dimension(90, 90));
        botonSalir.setPreferredSize(new java.awt.Dimension(90, 90));
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(boton_RegistrarAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(boton_ResponderConsulta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(boton_RegistrarAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(boton_ResponderConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 221, Short.MAX_VALUE)
                .addComponent(botonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void boton_RegistrarAlimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_RegistrarAlimentoActionPerformed
        framePrincipal.mostrarRegistrarAlimento();
    }//GEN-LAST:event_boton_RegistrarAlimentoActionPerformed

    private void boton_ResponderConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_ResponderConsultaActionPerformed
        framePrincipal.mostrarResponderConsulta();
    }//GEN-LAST:event_boton_ResponderConsultaActionPerformed

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        framePrincipal.mostrarLogIn();
        framePrincipal.mostrarBarraLateralLogIn();
        framePrincipal.vaciarPersona();
    }//GEN-LAST:event_botonSalirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonSalir;
    private javax.swing.JButton boton_RegistrarAlimento;
    private javax.swing.JButton boton_ResponderConsulta;
    // End of variables declaration//GEN-END:variables
}
