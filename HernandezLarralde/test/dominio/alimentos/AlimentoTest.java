/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Alimento.TipoAlimento;
import dominio.alimentos.Nutriente.Nutrientes;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class AlimentoTest {

    Alimento instance;

    public AlimentoTest() {
    }

    @Before
    public void setUp() {
        instance = new Alimento();
    }

    /**
     * Test of setNombre method, of class Alimento.
     */
    @Test
    public void testSetNombreVacio() {
        String nombre = "";
        instance.setNombre(nombre);

        String expResult = "";
        String result = instance.getNombre();

        assertEquals(result, expResult);
    }

    /**
     * Test of setNombre method, of class Alimento.
     */
    @Test
    public void testSetNombreNoVacio() {
        String nombre = "Nombre";
        instance.setNombre(nombre);

        String expResult = "Nombre";
        String result = instance.getNombre();

        assertEquals(result, expResult);
    }

    /**
     * Test of setTipoAlimento method, of class Alimento.
     */
    @Test
    public void testSetTipoAlimentoNulo() {
        instance.setTipoAlimento(null);

        ArrayList<TipoAlimento> expResult = null;
        ArrayList<TipoAlimento> result = instance.getTipoAlimento();

        assertEquals(result, expResult);
    }

    /**
     * Test of setTipoAlimento method, of class Alimento.
     */
    @Test
    public void testSetTipoAlimentoConUnElemento() {
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        TipoAlimento tipoAlimento = TipoAlimento.Bebida;
        tiposAlimento.add(tipoAlimento);
        instance.setTipoAlimento(tiposAlimento);

        ArrayList<TipoAlimento> expResult = tiposAlimento;
        ArrayList<TipoAlimento> result = instance.getTipoAlimento();

        assertEquals(result, expResult);
    }

    /**
     * Test of setNutrientes method, of class Alimento.
     */
    @Test
    public void testSetNutrientesNulo() {
        ArrayList<Nutriente> nutrientes = null;
        instance.setNutrientes(nutrientes);

        ArrayList<Nutriente> expResult = null;
        ArrayList<Nutriente> result = instance.getNutrientes();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setNutrientes method, of class Alimento.
     */
    @Test
    public void testSetNutrientesConUnElemento() {
        Nutrientes fibra = Nutrientes.Fibra;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(fibra);
        ArrayList<Nutriente> nutrientes = new ArrayList<Nutriente>();
        nutrientes.add(nutriente);
        instance.setNutrientes(nutrientes);

        ArrayList<Nutriente> expResult = nutrientes;
        ArrayList<Nutriente> result = instance.getNutrientes();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getNombre method, of class Alimento.
     */
    @Test
    public void testGetNombreVacio() {
        String expResult = "";
        String result = instance.getNombre();
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetNombreNoVacio() {
        instance.setNombre("Nombre");
        
        String expResult = "Nombre";
        String result = instance.getNombre();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getTipoAlimento method, of class Alimento.
     */
    @Test
    public void testGetTipoAlimentoDefault() {
        ArrayList<Alimento.TipoAlimento> expResult = new ArrayList<>();
        
        ArrayList<Alimento.TipoAlimento> result = instance.getTipoAlimento();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getTipoAlimento method, of class Alimento.
     */
    @Test
    public void testGetTipoAlimentoConUnElemento() {
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        TipoAlimento tipoAlimento = TipoAlimento.Bebida;
        tiposAlimento.add(tipoAlimento);
        instance.setTipoAlimento(tiposAlimento);
        
        ArrayList<Alimento.TipoAlimento> expResult = tiposAlimento;
        
        ArrayList<Alimento.TipoAlimento> result = instance.getTipoAlimento();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getNutrientes method, of class Alimento.
     */
    @Test
    public void testGetNutrientesDefault() {
        ArrayList<Nutriente> expResult = new ArrayList<>();
        
        ArrayList<Nutriente> result = instance.getNutrientes();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getNutrientes method, of class Alimento.
     */
    @Test
    public void testGetNutrientesConUnElemento() {
        Nutrientes fibra = Nutrientes.Fibra;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(fibra);
        ArrayList<Nutriente> nutrientes = new ArrayList<Nutriente>();
        nutrientes.add(nutriente);
        instance.setNutrientes(nutrientes);
        
        ArrayList<Nutriente> expResult = nutrientes;
        
        ArrayList<Nutriente> result = instance.getNutrientes();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of tieneTipoAlimento method, of class Alimento.
     */
    @Test
    public void testTieneTipoAlimentoFalse() {
        TipoAlimento Bebida = TipoAlimento.Bebida;
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(Bebida);
        
        TipoAlimento Carne = TipoAlimento.Carne;
        ArrayList<TipoAlimento> tiene = new ArrayList<>();
        tiene.add(Carne);
        instance.setTipoAlimento(tiene);
        
        boolean expResult = false;
        
        boolean result = instance.tieneTipoAlimento(tiposAlimento);
        
        assertEquals(expResult, result);
    }

     /**
     * Test of tieneTipoAlimento method, of class Alimento.
     */
    @Test
    public void testTieneTipoAlimentoTrue() {
        TipoAlimento Bebida = TipoAlimento.Bebida;
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(Bebida);
        
        TipoAlimento BebidaDos = TipoAlimento.Bebida;
        ArrayList<TipoAlimento> tiene = new ArrayList<>();
        tiene.add(BebidaDos);
        instance.setTipoAlimento(tiene);
        
        boolean expResult = true;
        
        boolean result = instance.tieneTipoAlimento(tiposAlimento);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of tieneNutrientes method, of class Alimento.
     */
    @Test
    public void testTieneNutrientesTrue() {
        Nutrientes tieneFibra = Nutrientes.Fibra;
        Nutriente tieneNutriente = new Nutriente();
        tieneNutriente.setNombre(tieneFibra);
        ArrayList<Nutriente> tieneNutrientes;
        tieneNutrientes = new ArrayList<>();
        tieneNutrientes.add(tieneNutriente);
        instance.setNutrientes(tieneNutrientes);
        
        Nutrientes fibra = Nutrientes.Fibra;
        ArrayList<Nutrientes> nutrientes;
        nutrientes = new ArrayList<>();
        nutrientes.add(fibra);
        
        boolean expResult = true;
        
        boolean result = instance.tieneNutrientes(nutrientes);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of tieneNutrientes method, of class Alimento.
     */
    @Test
    public void testTieneNutrientesFalse() {
        Nutrientes tieneFibra = Nutrientes.Fibra;
        Nutriente tieneNutriente = new Nutriente();
        tieneNutriente.setNombre(tieneFibra);
        ArrayList<Nutriente> tieneNutrientes;
        tieneNutrientes = new ArrayList<>();
        tieneNutrientes.add(tieneNutriente);
        instance.setNutrientes(tieneNutrientes);
        
        Nutrientes hierro = Nutrientes.Hierro;
        ArrayList<Nutrientes> nutrientes;
        nutrientes = new ArrayList<>();
        nutrientes.add(hierro);
        
        boolean expResult = false;
        
        boolean result = instance.tieneNutrientes(nutrientes);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Alimento.
     */
    @Test
    public void testToString() {
        instance.setNombre("Nombre");
        
        String expResult = "Nombre";
        
        String result = instance.toString();
        
        assertEquals(expResult, result);
    }

}
