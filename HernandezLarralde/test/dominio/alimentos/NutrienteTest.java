/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Nutriente.Nutrientes;
import dominio.alimentos.Nutriente.Unidades;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class NutrienteTest {
    
    Nutriente instance;
    
    public NutrienteTest() {
    }
    
    @Before
    public void setUp() {
        instance = new Nutriente();
    }

    /**
     * Test of setNombre method, of class Nutriente.
     */
    @Test
    public void testSetNombreNulo() {
        Nutrientes nombre = null;
        instance.setNombre(nombre);
        
        Nutrientes expResult = null;
        
        Nutrientes result = instance.getNombre();
        
        assertEquals(expResult, result);
    }
    
       /**
     * Test of setNombre method, of class Nutriente.
     */
    @Test
    public void testSetNombreNoNulo() {
        Nutrientes nombre = Nutrientes.Hierro;
        instance.setNombre(nombre);
        
        Nutrientes expResult = Nutrientes.Hierro;
        
        Nutrientes result = instance.getNombre();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of setValor method, of class Nutriente.
     */
    @Test
    public void testSetValor() {
        int valor = 0;
        instance.setValor(valor);
    }

    /**
     * Test of setUnidad method, of class Nutriente.
     */
    @Test
    public void testSetUnidad() {
        Unidades unidad = Unidades.g;
        instance.setUnidad(unidad);
    }

    /**
     * Test of getNombre method, of class Nutriente.
     */
    @Test
    public void testGetNombreNulo() {
        Nutrientes nombre = null;
        instance.setNombre(nombre);
        
        Nutrientes expResult = null;
        
        Nutrientes result = instance.getNombre();
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of getNombre method, of class Nutriente.
     */
    @Test
    public void testGetNombreNoNulo() {
        Nutrientes nombre = Nutrientes.Hierro;
        instance.setNombre(nombre);
        
        Nutrientes expResult = Nutrientes.Hierro;
        
        Nutrientes result = instance.getNombre();
        
        assertEquals(expResult, result);
    }
    
}
