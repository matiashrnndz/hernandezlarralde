/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.personas.Profesional;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class PlanAlimentacionSemanalTest {
    
    private PlanAlimentacionSemanal instance;
    private Profesional profesional;
    
    public PlanAlimentacionSemanalTest() {
    }
    
    @Before
    public void setUp() {
        profesional = new Profesional();
        instance = new PlanAlimentacionSemanal(profesional);
    }

    /**
     * Test of agregarAlimento method, of class PlanAlimentacionSemanal.
     */
    @Test
    public void testAgregarAlimentoEnPrimeraPos() {
        int indexComida = 0;
        int indexDia = 0;
        Alimento alimento = new Alimento();
        alimento.setNombre("Alimento");
        instance.agregarAlimento(indexComida, indexDia, alimento);
        
        String expResult;
        expResult = "Alimento";
        
        String result;
        result = instance.getNombreAlimentoPorPosicion(indexComida, indexDia);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of agregarAlimento method, of class PlanAlimentacionSemanal.
     */
    @Test
    public void testAgregarAlimentoEnUltimaPos() {
        int indexComida = 3;
        int indexDia = 6;
        Alimento alimento = new Alimento();
        alimento.setNombre("Alimento");
        instance.agregarAlimento(indexComida, indexDia, alimento);
        
        String expResult;
        expResult = "Alimento";
        
        String result;
        result = instance.getNombreAlimentoPorPosicion(indexComida, indexDia);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of agregarAlimento method, of class PlanAlimentacionSemanal.
     */
    @Test
    (expected = AssertionError.class)
    public void testAgregarAlimentoAfuera() {
        int indexComida = 4;
        int indexDia = 7;
        Alimento alimento = new Alimento();
        alimento.setNombre("Alimento");
        instance.agregarAlimento(indexComida, indexDia, alimento);
    }

    /**
     * Test of getNombreAlimentoPorPosicion method, of class PlanAlimentacionSemanal.
     */
    @Test
    public void testGetNombreAlimentoPorPosicionPrimeraPos() {
        int indexComida = 0;
        int indexDia = 0;
        Alimento alimento = new Alimento();
        alimento.setNombre("Alimento");
        instance.agregarAlimento(indexComida, indexDia, alimento);
        
        String expResult;
        expResult = "Alimento";
        
        String result;
        result = instance.getNombreAlimentoPorPosicion(indexComida, indexDia);
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of getNombreAlimentoPorPosicion method, of class PlanAlimentacionSemanal.
     */
    @Test
    public void testGetNombreAlimentoPorPosicionUltimaPos() {
        int indexComida = 3;
        int indexDia = 6;
        Alimento alimento = new Alimento();
        alimento.setNombre("Alimento");
        instance.agregarAlimento(indexComida, indexDia, alimento);
        
        String expResult;
        expResult = "Alimento";
        
        String result;
        result = instance.getNombreAlimentoPorPosicion(indexComida, indexDia);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getNombreAlimentoPorPosicion method, of class PlanAlimentacionSemanal.
     */
    @Test
    public void testGetNombreAlimentoPorPosicionAfuera() {
        int indexComida = 4;
        int indexDia = 7;
        
        String expResult;
        expResult = "";
        
        String result;
        result = instance.getNombreAlimentoPorPosicion(indexComida, indexDia);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getProfesional method, of class PlanAlimentacionSemanal.
     */
    @Test
    public void testGetProfesionalDefault() {
        Profesional expResult = profesional;
        
        Profesional result = instance.getProfesional();
        
        assertEquals(expResult, result);
    }
    
}
