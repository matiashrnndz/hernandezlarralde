/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Alimento.TipoAlimento;
import dominio.alimentos.Nutriente.Nutrientes;
import dominio.repositorio.Repositorio;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class RegistroAlimentoTest {

    RegistroAlimento instance;
    Repositorio repositorio;

    public RegistroAlimentoTest() {
    }

    @Before
    public void setUp() {
        repositorio = new Repositorio();
        instance = new RegistroAlimento();
    }

    /**
     * Test of registrarNombre method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarNombreVacio() {
        String nombre = "";

        instance.registrarNombre(nombre);
        instance.registrarAlimentoEnRepositorio(repositorio);
        String result;
        result = repositorio.getAlimentos().get(0).getNombre();

        Alimento alimento = new Alimento();
        alimento.setNombre("");
        String expResult;
        expResult = alimento.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNombre method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarNombreNoVacio() {
        String nombre = "Nombre";

        instance.registrarNombre(nombre);
        instance.registrarAlimentoEnRepositorio(repositorio);
        String result;
        result = repositorio.getAlimentos().get(0).getNombre();

        Alimento alimento = new Alimento();
        alimento.setNombre("Nombre");
        String expResult;
        expResult = alimento.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarTiposAlimento method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarTiposAlimentoConUnElemento() {
        TipoAlimento tipoAlimento = TipoAlimento.Bebida;
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(tipoAlimento);
        instance.registrarTiposAlimento(tiposAlimento);

        instance.registrarAlimentoEnRepositorio(repositorio);
        TipoAlimento result;
        result = repositorio.getAlimentos().get(0).getTipoAlimento().get(0);

        Alimento alimento = new Alimento();
        alimento.setTipoAlimento(tiposAlimento);
        TipoAlimento expResult;
        expResult = alimento.getTipoAlimento().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarTiposAlimento method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarTiposAlimentoNulo() {
        TipoAlimento tipoAlimento = null;
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(tipoAlimento);
        instance.registrarTiposAlimento(tiposAlimento);

        instance.registrarAlimentoEnRepositorio(repositorio);
        TipoAlimento result;
        result = repositorio.getAlimentos().get(0).getTipoAlimento().get(0);

        Alimento alimento = new Alimento();
        alimento.setTipoAlimento(tiposAlimento);
        TipoAlimento expResult;
        expResult = alimento.getTipoAlimento().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNutrientes method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarNutrientesNulo() {
        ArrayList<Nutriente> nutrientes = null;
        instance.registrarNutrientes(nutrientes);

        instance.registrarAlimentoEnRepositorio(repositorio);
        ArrayList<Nutriente> result;
        result = repositorio.getAlimentos().get(0).getNutrientes();

        ArrayList<Nutriente> expResult;
        expResult = nutrientes;

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNutrientes method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarNutrientesConUnElemento() {
        Nutrientes Hierro = Nutrientes.Hierro;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(Hierro);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        instance.registrarNutrientes(nutrientes);

        instance.registrarAlimentoEnRepositorio(repositorio);
        ArrayList<Nutriente> result;
        result = repositorio.getAlimentos().get(0).getNutrientes();

        ArrayList<Nutriente> expResult;
        expResult = nutrientes;

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarAlimentoEnRepositorio method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarAlimentoEnRepositorioDefault() {
        instance.registrarAlimentoEnRepositorio(repositorio);

        int expResult;
        expResult = 1;

        int result;
        result = repositorio.getAlimentos().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarAlimentoEnRepositorio method, of class RegistroAlimento.
     */
    @Test
    public void testRegistrarAlimentoEnRepositorio() {
        String nombre = "Nombre";
        instance.registrarNombre("Nombre");

        Nutrientes Hierro = Nutrientes.Hierro;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(Hierro);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        instance.registrarNutrientes(nutrientes);

        TipoAlimento tipoAlimento = TipoAlimento.Bebida;
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(tipoAlimento);
        instance.registrarTiposAlimento(tiposAlimento);

        instance.registrarAlimentoEnRepositorio(repositorio);

        String expNombre;
        expNombre = nombre;

        ArrayList<Nutriente> expNutrientes;
        expNutrientes = nutrientes;

        Alimento alimento = new Alimento();
        alimento.setTipoAlimento(tiposAlimento);
        TipoAlimento expTipoAlimento;
        expTipoAlimento = alimento.getTipoAlimento().get(0);

        Alimento registrado = repositorio.getAlimentos().get(0);
        
        ArrayList<Nutriente> resultNutriente;
        resultNutriente = registrado.getNutrientes();

        TipoAlimento resultTipoAlimento;
        resultTipoAlimento = registrado.getTipoAlimento().get(0);

        String resultNombre;
        resultNombre = registrado.getNombre();

        assertTrue(expNombre.equals(resultNombre)
                && expNutrientes == resultNutriente
                && expTipoAlimento == resultTipoAlimento);
    }

}
