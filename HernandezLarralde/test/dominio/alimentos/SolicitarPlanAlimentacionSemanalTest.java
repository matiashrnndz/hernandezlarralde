/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.alimentos;

import dominio.alimentos.Alimento.TipoAlimento;
import dominio.alimentos.Nutriente.Nutrientes;
import dominio.personas.Profesional;
import dominio.personas.Usuario;
import dominio.repositorio.Repositorio;
import dominio.repositorio.Repositorio.PreferenciasAlimenticias;
import dominio.repositorio.Repositorio.RestriccionesAlimenticias;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class SolicitarPlanAlimentacionSemanalTest {

    private SolicitarPlanAlimentacionSemanal instance;
    private Usuario usuario;
    private Profesional profesional;
    private Repositorio repositorio;

    public SolicitarPlanAlimentacionSemanalTest() {
    }

    @Before
    public void setUp() {
        usuario = new Usuario();
        profesional = new Profesional();
        repositorio = new Repositorio();
        instance = new SolicitarPlanAlimentacionSemanal(usuario, profesional, repositorio);
    }

    /**
     * Test of crearPlan method, of class SolicitarPlanAlimentacionSemanal.
     */
    @Test
    public void testCrearPlanConUnAlimentoRegistrado() {
        Alimento alimento = new Alimento();
        alimento.setNombre("Arroz");
        repositorio.agregarAlimento(alimento);

        instance.crearPlan();
        instance.registrarPlanAUsuario();

        PlanAlimentacionSemanal plan = usuario.getPlanAlimentacionSemanalDe(profesional);

        int expResult;
        expResult = 28;

        int cantArroz;
        cantArroz = 0;

        for (int f = 0; f < 4; f++) {
            for (int c = 0; c < 7; c++) {
                if (plan.getNombreAlimentoPorPosicion(f, c).equals("Arroz")) {
                    cantArroz++;
                }
            }
        }
        
        int result;
        result = cantArroz;

        assertEquals(expResult, result);
    }
    
    /**
     * Test of crearPlan method, of class SolicitarPlanAlimentacionSemanal.
     */
    @Test
    public void testCrearPlanConUnElementoRestringidoYUnoPermitido() {
        RestriccionesAlimenticias restriccion = RestriccionesAlimenticias.Hipertenso;
        ArrayList<RestriccionesAlimenticias> restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        usuario.setRestriccionesAlimenticias(restricciones);
        
        Alimento arroz = new Alimento();
        Nutrientes sodio = Nutrientes.Sodio;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(sodio);
        nutriente.setValor(2);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        arroz.setNutrientes(nutrientes);
        arroz.setNombre("Arroz");
        repositorio.agregarAlimento(arroz);
        
        Alimento bebida = new Alimento();
        bebida.setNombre("CocaCola");
        repositorio.agregarAlimento(bebida);
        
        instance.crearPlan();
        instance.registrarPlanAUsuario();

        PlanAlimentacionSemanal plan = usuario.getPlanAlimentacionSemanalDe(profesional);

        boolean expResult;
        expResult = false;

        boolean poseeAlimentoNoPermitido;
        poseeAlimentoNoPermitido = false;

        for (int f = 0; f < 4; f++) {
            for (int c = 0; c < 7; c++) {
                if (plan.getNombreAlimentoPorPosicion(f, c).equals("Arroz")) {
                    poseeAlimentoNoPermitido = true;
                }
            }
        }
        
        boolean result;
        result = poseeAlimentoNoPermitido;

        assertEquals(expResult, result);
    }
    
    /**
     * Test of crearPlan method, of class SolicitarPlanAlimentacionSemanal.
     */
    @Test
    public void testCrearPlanConUnElementoSinPreferenciaYUnoPermitido() {
        PreferenciasAlimenticias preferencia = PreferenciasAlimenticias.Vegetariano;
        ArrayList<PreferenciasAlimenticias> preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        usuario.setPreferenciasAlimenticias(preferencias);
        
        Alimento milanesa = new Alimento();
        TipoAlimento carne = TipoAlimento.Carne;
        ArrayList<TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(carne);
        milanesa.setTipoAlimento(tiposAlimento);
        milanesa.setNombre("Milanesa");
        repositorio.agregarAlimento(milanesa);
        
        Alimento bebida = new Alimento();
        bebida.setNombre("CocaCola");
        repositorio.agregarAlimento(bebida);
        
        instance.crearPlan();
        instance.registrarPlanAUsuario();

        PlanAlimentacionSemanal plan = usuario.getPlanAlimentacionSemanalDe(profesional);

        boolean expResult;
        expResult = false;

        boolean poseeAlimentoNoPermitido;
        poseeAlimentoNoPermitido = false;

        for (int f = 0; f < 4; f++) {
            for (int c = 0; c < 7; c++) {
                if (plan.getNombreAlimentoPorPosicion(f, c).equals("Milanesa")) {
                    poseeAlimentoNoPermitido = true;
                }
            }
        }
        
        boolean result;
        result = poseeAlimentoNoPermitido;

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarPlanAUsuario method, of class
     * SolicitarPlanAlimentacionSemanal.
     */
    @Test
    public void testRegistrarPlanAUsuarioConPlanExistente() {
        Alimento alimento = new Alimento();
        alimento.setNombre("Arroz");
        repositorio.agregarAlimento(alimento);

        instance.crearPlan();
        instance.registrarPlanAUsuario();

        instance.crearPlan();
        instance.registrarPlanAUsuario();
        
        PlanAlimentacionSemanal plan = usuario.getPlanAlimentacionSemanalDe(profesional);

        int expResult;
        expResult = 28;

        int cantArroz;
        cantArroz = 0;

        for (int f = 0; f < 4; f++) {
            for (int c = 0; c < 7; c++) {
                if (plan.getNombreAlimentoPorPosicion(f, c).equals("Arroz")) {
                    cantArroz++;
                }
            }
        }
        
        int result;
        result = cantArroz;

        assertEquals(expResult, result);
    }

}
