/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.consulta;

import dominio.personas.Profesional;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ConsultaTest {

    Consulta instance;
    Consulta instanceConProfesional;
    Profesional profesional;

    public ConsultaTest() {
    }

    @Before
    public void setUp() {
        instance = new Consulta();
        profesional = new Profesional();
        instanceConProfesional = new Consulta(profesional);
    }

    /**
     * Test of consultar method, of class Consulta.
     */
    @Test
    public void testConsultarVacio() {
        String consulta = "";

        instance.consultar(consulta);
    }

    /**
     * Test of consultar method, of class Consulta.
     */
    @Test
    public void testConsultaConProfesionalInicializadoStringVacio() {
        Profesional expResult;
        expResult = profesional;
        
        Profesional result;
        result = instanceConProfesional.getProfesional();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of consultar method, of class Consulta.
     */
    @Test
    public void testConsultarNoVacio() {
        String TextoConsulta = "Consulta";

        instance.consultar(TextoConsulta);
    }

    /**
     * Test of responder method, of class Consulta.
     */
    @Test
    public void testResponderVacio() {
        String respuesta = "";

        instance.responder(respuesta);
    }

    /**
     * Test of responder method, of class Consulta.
     */
    @Test
    public void testResponderNoVacio() {
        String respuesta = "Consulta";

        instance.responder(respuesta);
    }

    /**
     * Test of getConsulta method, of class Consulta.
     */
    @Test
    public void testGetConsultaVacia() {
        String expResult = "";

        instance.consultar(expResult);
        String result = instance.getConsulta();

        assertEquals(expResult, result);
    }

    /**
     * Test of getConsulta method, of class Consulta.
     */
    @Test
    public void testGetConsultaNoVacia() {
        String expResult = "Consulta";

        instance.consultar(expResult);
        String result = instance.getConsulta();

        assertEquals(expResult, result);
    }

    /**
     * Test of getProfesional method, of class Consulta.
     */
    @Test
    public void testGetProfesional() {
        Profesional expResult = null;

        Profesional result = instance.getProfesional();

        assertEquals(expResult, result);
    }

    /**
     * Test of getHistorial method, of class Consulta.
     */
    @Test
    public void testGetHistorial() {
        String expResult = "Usuario : " + "" + "\n \n";

        instance.consultar("");
        String result = instance.getHistorial();

        assertEquals(expResult, result);
    }

}
