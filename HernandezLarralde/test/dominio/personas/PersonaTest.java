/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class PersonaTest {

    private PersonaImpl instance;

    public PersonaTest() {

    }

    @Before
    public void setUp() {
        instance = new PersonaImpl();
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testSetNombreVacio() {
        instance.setNombre("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testSetNombreNoVacio() {
        instance.setNombre("Jorge");

        String expResult;
        expResult = "Jorge";

        String result;
        result = instance.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of setApellido method, of class Persona.
     */
    @Test
    public void testSetApellidoVacio() {
        instance.setApellido("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getApellido();

        assertEquals(expResult, result);
    }

    /**
     * Test of setApellido method, of class Persona.
     */
    @Test
    public void testSetApellidoNoVacio() {
        instance.setApellido("Ruperto");

        String expResult;
        expResult = "Ruperto";

        String result;
        result = instance.getApellido();

        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaNacimiento method, of class Persona.
     */
    @Test
    public void testSetFechaNacimientoNoNula() {
        Date fechaNacimiento = new Date(0, 0, 0);
        instance.setFechaNacimiento(fechaNacimiento);

        Date expResult = fechaNacimiento;

        Date result = instance.getFechaNacimiento();

        assertEquals(expResult, result);

    }

    /**
     * Test of setFechaNacimiento method, of class Persona.
     */
    @Test
    public void testSetFechaNacimientoNula() {
        Date fechaNacimiento = null;
        instance.setFechaNacimiento(fechaNacimiento);

        Date result = instance.getFechaNacimiento();

        assertNull(result);
    }

    /**
     * Test of setFotoPerfil method, of class Persona.
     */
    @Test
    public void testSetFotoPerfilNula() {
        BufferedImage fotoPerfil = null;
        instance.setFotoPerfil(fotoPerfil);

        BufferedImage result = instance.getFotoPerfil();

        assertNull(result);
    }

    /**
     * Test of setFotoPerfil method, of class Persona.
     */
    @Test
    public void testSetFotoPerfilNoNula(){
        BufferedImage fotoPerfil;

        try {
            fotoPerfil = ImageIO.read(new File("/interfaz_usuario/imagenes/AgregarComida90px.png"));
        } catch (IOException e) {
            fotoPerfil = null;
        }
        
        instance.setFotoPerfil(fotoPerfil);

        BufferedImage expResult = fotoPerfil;

        BufferedImage result = instance.getFotoPerfil();

        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testGetNombreNoVacio() {
        instance.setNombre("Jorge");

        String expResult;
        expResult = "Jorge";

        String result;
        result = instance.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testGetNombreVacio() {
        instance.setNombre("Jorge");

        String expResult;
        expResult = "Jorge";

        String result;
        result = instance.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of getApellido method, of class Persona.
     */
    @Test
    public void testGetApellidoNoVacio() {
        instance.setApellido("Ruperto");

        String expResult;
        expResult = "Ruperto";

        String result;
        result = instance.getApellido();

        assertEquals(expResult, result);
    }

    /**
     * Test of getApellido method, of class Persona.
     */
    @Test
    public void testGetApellidoVacio() {
        instance.setApellido("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getApellido();

        assertEquals(expResult, result);
    }

    /**
     * Test of getFechaNacimiento method, of class Persona.
     */
    @Test
    public void testGetFechaNacimientoNoNula() {
        Date fechaNacimiento = new Date(0, 0, 0);
        instance.setFechaNacimiento(fechaNacimiento);

        Date expResult = fechaNacimiento;

        Date result = instance.getFechaNacimiento();

        assertEquals(expResult, result);

    }

    /**
     * Test of getFechaNacimiento method, of class Persona.
     */
    @Test
    public void testGetFechaNacimientoNula() {
        Date fechaNacimiento = null;
        instance.setFechaNacimiento(fechaNacimiento);

        Date result = instance.getFechaNacimiento();

        assertNull(result);
    }

    /**
     * Test of getFotoPerfil method, of class Persona.
     */
    @Test
    public void testGetFotoPerfilNula() {
        BufferedImage fotoPerfil = null;
        instance.setFotoPerfil(fotoPerfil);

        BufferedImage result = instance.getFotoPerfil();

        assertNull(result);

    }

    /**
     * Test of getFotoPerfil method, of class Persona.
     */
    @Test
    public void testGetFotoPerfilNoNula() {
         BufferedImage fotoPerfil;

        try {
            fotoPerfil = ImageIO.read(new File("/interfaz_usuario/imagenes/AgregarComida90px.png"));
        } catch (IOException e) {
            fotoPerfil = null;
        }
        
        instance.setFotoPerfil(fotoPerfil);

        BufferedImage expResult = fotoPerfil;

        BufferedImage result = instance.getFotoPerfil();

        assertEquals(expResult, result);
    }
    
    public class PersonaImpl extends Persona {

    }

}
