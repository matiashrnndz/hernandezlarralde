/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ProfesionalTest {

    Profesional instance;

    public ProfesionalTest() {
    }

    @Before
    public void setUp() {
        instance = new Profesional();
    }

    /**
     * Test of setTitulo method, of class Profesional.
     */
    @Test
    public void testSetTituloNoVacio() {
        instance.setTitulo("Nutricionista");

        String expResult;
        expResult = "Nutricionista";

        String result;
        result = instance.getTitulo();

        assertEquals(expResult, result);
    }

    /**
     * Test of setTitulo method, of class Profesional.
     */
    @Test
    public void testSetTituloVacio() {
        instance.setTitulo("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getTitulo();

        assertEquals(expResult, result);
    }

     /**
     * Test of setFechaTitulo method, of class Profesional.
     */
    @Test
    public void testSetFechaTituloNoNula() {
        Date fechaTitulo = new Date(0, 0, 0);
        instance.setFechaTitulo(fechaTitulo);
        
        Date expResult = fechaTitulo;
        
        Date result = instance.getFechaTitulo();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setFechaTitulo method, of class Profesional.
     */
    @Test
    public void testSetFechaTituloNula() {
        Date fechaTitulo = null;
        instance.setFechaTitulo(fechaTitulo);
        
        Date result = instance.getFechaTitulo();
        
        assertNull(result);
    }
    
    /**
     * Test of setPais method, of class Profesional.
     */
    @Test
    public void testSetPaisVacio() {
        instance.setPais("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getPais();

        assertEquals(expResult, result);
    }

    /**
     * Test of setPais method, of class Profesional.
     */
    @Test
    public void testSetPaisNoVacio() {
        instance.setPais("Uruguay");

        String expResult;
        expResult = "Uruguay";

        String result;
        result = instance.getPais();

        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Profesional.
     */
    @Test
    public void testSetIdNegativo() {
        instance.setId(-1);

        int expResult;
        expResult = -1;

        int result;
        result = instance.getId();

        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Profesional.
     */
    @Test
    public void testSetIdCero() {
        instance.setId(0);

        int expResult;
        expResult = 0;

        int result;
        result = instance.getId();

        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Profesional.
     */
    @Test
    public void testSetIdPositivo() {
        instance.setId(1);

        int expResult;
        expResult = 1;

        int result;
        result = instance.getId();

        assertEquals(expResult, result);
    }

    /**
     * Test of getTitulo method, of class Profesional.
     */
    @Test
    public void testGetTituloNoVacio() {
        instance.setTitulo("Nutricionista");

        String expResult;
        expResult = "Nutricionista";

        String result;
        result = instance.getTitulo();

        assertEquals(expResult, result);
    }

    /**
     * Test of getTitulo method, of class Profesional.
     */
    @Test
    public void testGetTituloVacio() {
        instance.setTitulo("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getTitulo();

        assertEquals(expResult, result);
    }

    /**
     * Test of getFechaTitulo method, of class Profesional.
     */
    @Test
    public void testGetFechaTituloNoNula() {
        Date fechaTitulo = new Date(0, 0, 0);
        instance.setFechaTitulo(fechaTitulo);
        
        Date expResult = fechaTitulo;
        
        Date result = instance.getFechaTitulo();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getFechaTitulo method, of class Profesional.
     */
    @Test
    public void testGetFechaTituloNula() {
        Date fechaTitulo = null;
        instance.setFechaTitulo(fechaTitulo);
        
        Date result = instance.getFechaTitulo();
        
        assertNull(result);
    }

    /**
     * Test of getPais method, of class Profesional.
     */
    @Test
    public void testGetPaisNoVacio() {
        instance.setPais("Uruguay");

        String expResult;
        expResult = "Uruguay";

        String result;
        result = instance.getPais();

        assertEquals(expResult, result);
    }

    /**
     * Test of getPais method, of class Profesional.
     */
    @Test
    public void testGetPaisVacio() {
        instance.setPais("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getPais();

        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Profesional.
     */
    @Test
    public void testGetIdNegativo() {
        instance.setId(-1);

        int expResult;
        expResult = -1;

        int result;
        result = instance.getId();

        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Profesional.
     */
    @Test
    public void testGetIdCero() {
        instance.setId(0);

        int expResult;
        expResult = 0;

        int result;
        result = instance.getId();

        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Profesional.
     */
    @Test
    public void testGetIdPositivo() {
        instance.setId(1);

        int expResult;
        expResult = 1;

        int result;
        result = instance.getId();

        assertEquals(expResult, result);
    }
}
