/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import dominio.repositorio.Repositorio;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class RegistroProfesionalTest {
    
    RegistroProfesional instance;
    
    public RegistroProfesionalTest() {
    }
    
    @Before
    public void setUp() {
        instance = new RegistroProfesional();
    }

    /**
     * Test of getProfesional method, of class RegistroProfesional.
     */
    @Test
    public void testGetProfesionalNoNulo() {
        Profesional result;
        result = instance.getProfesional();
        
        assertNotNull(result);
    }

    /**
     * Test of registrarNombre method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarNombreNoVacio() {
        instance.registrarNombre("Jorge");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "Jorge";
        
        String result = profesional.getNombre();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNombre method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarNombreVacio() {
        instance.registrarNombre("");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "";
        
        String result = profesional.getNombre();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarApellido method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarApellidoNoVacio() {
        instance.registrarApellido("Camp");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "Camp";
        
        String result = profesional.getApellido();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarApellido method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarApellidoVacio() {
        instance.registrarApellido("");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "";
        
        String result = profesional.getApellido();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNombreTitulo method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarNombreTituloNoVacio() {
        instance.registrarNombreTitulo("Nutricionista");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "Nutricionista";
        
        String result = profesional.getTitulo();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNombreTitulo method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarNombreTituloVacio() {
        instance.registrarNombreTitulo("");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "";
        
        String result = profesional.getTitulo();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarFechaTitulo method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarFechaTituloNoNulo() {
        Date fechaTitulo = new Date(0, 0, 0);
        instance.registrarFechaTitulo(fechaTitulo);
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        Date expResult = fechaTitulo;
        
        Date result = profesional.getFechaTitulo();
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of registrarFechaTitulo method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarFechaTituloNulo() {
        Date fechaTitulo = null;
        instance.registrarFechaTitulo(fechaTitulo);
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        Date result = profesional.getFechaTitulo();
        
        assertNull(result);
    }
    
    /**
     * Test of registrarPaisTitulo method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarPaisTituloNoVacio() {
        instance.registrarPaisTitulo("Uruguay");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "Uruguay";
        
        String result = profesional.getPais();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarPaisTitulo method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarPaisTituloVacio() {
        instance.registrarPaisTitulo("");
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        String expResult = "";
        
        String result = profesional.getPais();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNacimiento method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarNacimientoNoNulo() {
        Date fechaNacimiento = new Date(0, 0, 0);
        instance.registrarNacimiento(fechaNacimiento);
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        Date expResult = fechaNacimiento;
        
        Date result = profesional.getFechaNacimiento();
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of registrarNacimiento method, of class RegistroProfesional.
     */
    @Test
    public void testRegistrarNacimientoNulo() {
        Date fechaNacimiento = null;
        instance.registrarNacimiento(fechaNacimiento);
        
        Profesional profesional;
        profesional = instance.getProfesional();
        
        Date result = profesional.getFechaNacimiento();
        
        assertNull(result);
    }
    
    /**
     * Test of registrarProfesionalEnRepositorio method, of class
     * RegistroProfesional.
     */
    @Test
    public void testRegistrarProfesionalEnRepositorio() {
        Repositorio repositorio;
        repositorio = new Repositorio();
        
        Date fechaTitulo = new Date(0, 0, 0);
        Date fechaNacimiento = new Date(0, 0, 0);
        
        instance.registrarNombre("Jorge");
        instance.registrarApellido("Camp");
        instance.registrarNombreTitulo("Nutricionista");
        instance.registrarPaisTitulo("Uruguay");
        instance.registrarFechaTitulo(fechaTitulo);
        instance.registrarNacimiento(fechaNacimiento);
        instance.registrarProfesionalEnRepositorio(repositorio);
        
        String expNombre;
        expNombre = "Jorge";
        
        String expApellido;
        expApellido = "Camp";
        
        String expNombreTitulo;
        expNombreTitulo = "Nutricionista";
        
        String expPaisTitulo;
        expPaisTitulo = "Uruguay";
        
        Date expFechaTitulo;
        expFechaTitulo = fechaTitulo;
        
        Date expFechaNacimiento;
        expFechaNacimiento = fechaNacimiento;
        
        Profesional profesional;
        profesional = repositorio.getProfesionales().get(0);
        
        String resultNombre;
        resultNombre = profesional.getNombre();
        
        String resultApellido;
        resultApellido = profesional.getApellido();
        
        String resultNombreTitulo;
        resultNombreTitulo = profesional.getTitulo();
        
        String resultPaisTitulo;
        resultPaisTitulo = profesional.getPais();
        
        Date resultFechaTitulo;
        resultFechaTitulo = profesional.getFechaTitulo();
        
        Date resultFechaNacimiento;
        resultFechaNacimiento = profesional.getFechaNacimiento();
        
        assertTrue(expNombre.equals(resultNombre)
                && expApellido.equals(resultApellido)
                && expNombreTitulo.equals(resultNombreTitulo)
                && expPaisTitulo.equals(resultPaisTitulo)
                && expFechaNacimiento.equals(resultFechaNacimiento)
                && expFechaTitulo.equals(resultFechaTitulo));
        
    }
    
}
