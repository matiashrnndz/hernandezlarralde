/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import dominio.repositorio.Repositorio;
import dominio.repositorio.Repositorio.PreferenciasAlimenticias;
import dominio.repositorio.Repositorio.RestriccionesAlimenticias;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class RegistroUsuarioTest {

    RegistroUsuario instance;

    public RegistroUsuarioTest() {
    }

    @Before
    public void setUp() {
        instance = new RegistroUsuario();
    }

    /**
     * Test of getUsuario method, of class RegistroUsuario.
     */
    @Test
    public void testGetUsuario() {
        Usuario result;
        result = instance.getUsuario();

        assertNotNull(result);
    }

    /**
     * Test of registrarNombre method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarNombreNoVacio() {
        instance.registrarNombre("Jorge");

        Usuario usuario;
        usuario = instance.getUsuario();

        String expResult = "Jorge";

        String result = usuario.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNombre method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarNombreVacio() {
        instance.registrarNombre("");

        Usuario usuario;
        usuario = instance.getUsuario();

        String expResult = "";

        String result = usuario.getNombre();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarApellido method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarApellidoNoVacio() {
        instance.registrarApellido("Camp");

        Usuario usuario;
        usuario = instance.getUsuario();

        String expResult = "Camp";

        String result = usuario.getApellido();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarApellido method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarApellidoVacio() {
        instance.registrarApellido("");

        Usuario usuario;
        usuario = instance.getUsuario();

        String expResult = "";

        String result = usuario.getApellido();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNacionalidad method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarNacionalidadNoVacio() {
        instance.registrarNacionalidad("Uruguay");

        Usuario usuario;
        usuario = instance.getUsuario();

        String expResult = "Uruguay";

        String result = usuario.getNacionalidad();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNacionalidad method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarNacionalidadVacio() {
        instance.registrarNacionalidad("");

        Usuario usuario;
        usuario = instance.getUsuario();

        String expResult = "";

        String result = usuario.getNacionalidad();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarPreferencias method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarPreferenciasVacia() {
        ArrayList<PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        instance.registrarPreferencias(preferencias);

        Usuario usuario;
        usuario = instance.getUsuario();

        int expResult;
        expResult = 0;

        int result;
        result = usuario.getPreferencias().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarPreferencias method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarPreferenciasNula() {
        ArrayList<PreferenciasAlimenticias> preferencias;
        preferencias = null;

        instance.registrarPreferencias(preferencias);

        Usuario usuario;
        usuario = instance.getUsuario();

        ArrayList<PreferenciasAlimenticias> result;
        result = usuario.getPreferencias();

        assertNull(result);
    }

    /**
     * Test of registrarPreferencias method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarPreferenciasConUnElemento() {
        PreferenciasAlimenticias preferencia;
        preferencia = PreferenciasAlimenticias.Vegano;
        ArrayList<PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        instance.registrarPreferencias(preferencias);

        Usuario usuario;
        usuario = instance.getUsuario();

        PreferenciasAlimenticias expResult;
        expResult = PreferenciasAlimenticias.Vegano;

        PreferenciasAlimenticias result;
        result = usuario.getPreferencias().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarRestricciones method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarRestriccionesVacia() {
        ArrayList<RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        instance.registrarRestricciones(restricciones);

        Usuario usuario;
        usuario = instance.getUsuario();

        int expResult;
        expResult = 0;

        int result;
        result = usuario.getRestricciones().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarRestricciones method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarRestriccionesNula() {
        ArrayList<RestriccionesAlimenticias> restricciones;
        restricciones = null;

        instance.registrarRestricciones(restricciones);

        Usuario usuario;
        usuario = instance.getUsuario();

        ArrayList<RestriccionesAlimenticias> result;
        result = usuario.getRestricciones();

        assertNull(result);
    }

    /**
     * Test of registrarRestricciones method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarRestriccionesConUnElemento() {
        RestriccionesAlimenticias restriccion;
        restriccion = RestriccionesAlimenticias.Hipertenso;
        ArrayList<RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        instance.registrarRestricciones(restricciones);

        Usuario usuario;
        usuario = instance.getUsuario();

        RestriccionesAlimenticias expResult;
        expResult = RestriccionesAlimenticias.Hipertenso;

        RestriccionesAlimenticias result;
        result = usuario.getRestricciones().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNacimiento method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarNacimientoNoNulo() {
        Date fechaNacimiento = new Date(0, 0, 0);
        instance.registrarNacimiento(fechaNacimiento);

        Usuario usuario;
        usuario = instance.getUsuario();

        Date expResult = fechaNacimiento;

        Date result = usuario.getFechaNacimiento();

        assertEquals(expResult, result);
    }

    /**
     * Test of registrarNacimiento method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarNacimientoNulo() {
        Date fechaNacimiento = null;
        instance.registrarNacimiento(fechaNacimiento);

        Usuario usuario;
        usuario = instance.getUsuario();

        Date result = usuario.getFechaNacimiento();

        assertNull(result);
    }

    /**
     * Test of registrarUsuarioEnRepositorio method, of class RegistroUsuario.
     */
    @Test
    public void testRegistrarUsuarioEnRepositorio() {
        Repositorio repositorio;
        repositorio = new Repositorio();

        instance.registrarNombre("Jorge");
        instance.registrarApellido("Camp");
        instance.registrarNacionalidad("Uruguay");

        Date fechaNacimiento = new Date(0, 0, 0);
        instance.registrarNacimiento(fechaNacimiento);

        RestriccionesAlimenticias restriccion;
        restriccion = RestriccionesAlimenticias.Hipertenso;
        ArrayList<RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        instance.registrarRestricciones(restricciones);

        PreferenciasAlimenticias preferencia;
        preferencia = PreferenciasAlimenticias.Vegano;
        ArrayList<PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        instance.registrarPreferencias(preferencias);

        instance.registrarUsuarioEnRepositorio(repositorio);

        String expNombre;
        expNombre = "Jorge";

        String expApellido;
        expApellido = "Camp";

        String expNacionalidad;
        expNacionalidad = "Uruguay";

        Date expFechaNacimiento;
        expFechaNacimiento = fechaNacimiento;

        RestriccionesAlimenticias expRestriccion;
        expRestriccion = RestriccionesAlimenticias.Hipertenso;

        PreferenciasAlimenticias expPreferencias;
        expPreferencias = PreferenciasAlimenticias.Vegano;

        Usuario usuario;
        usuario = repositorio.getUsuarios().get(0);

        String resultNombre;
        resultNombre = usuario.getNombre();

        String resultApellido;
        resultApellido = usuario.getApellido();

        String resultNacionalidad;
        resultNacionalidad = usuario.getNacionalidad();

        Date resultFechaNacimiento;
        resultFechaNacimiento = usuario.getFechaNacimiento();
        
        RestriccionesAlimenticias resultRestriccion;
        resultRestriccion = usuario.getRestricciones().get(0);

        PreferenciasAlimenticias resultPreferencias;
        resultPreferencias = usuario.getPreferencias().get(0);

        assertTrue(expNombre.equals(resultNombre)
                && expApellido.equals(resultApellido)
                && expFechaNacimiento.equals(resultFechaNacimiento)
                && expNacionalidad.equals(resultNacionalidad)
                && expRestriccion.equals(resultRestriccion)
                && expPreferencias.equals(resultPreferencias));
    }

}
