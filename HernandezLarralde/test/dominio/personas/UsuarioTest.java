/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.personas;

import dominio.alimentos.Alimento;
import dominio.alimentos.PlanAlimentacionSemanal;
import dominio.consulta.Consulta;
import dominio.repositorio.Repositorio;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class UsuarioTest {

    Usuario instance;

    public UsuarioTest() {
    }

    @Before
    public void setUp() {
        instance = new Usuario();
    }

    /**
     * Test of setNacionalidad method, of class Usuario.
     */
    @Test
    public void testSetNacionalidadVacia() {
        instance.setNacionalidad("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getNacionalidad();

        assertEquals(expResult, result);
    }

    /**
     * Test of setNacionalidad method, of class Usuario.
     */
    @Test
    public void testSetNacionalidadNoVacia() {
        instance.setNacionalidad("Uruguay");

        String expResult;
        expResult = "Uruguay";

        String result;
        result = instance.getNacionalidad();

        assertEquals(expResult, result);
    }

    /**
     * Test of setPreferenciasAlimenticias method, of class Usuario.
     */
    @Test
    public void testSetPreferenciasAlimenticiasVacia() {
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        instance.setPreferenciasAlimenticias(preferencias);

        int expResult;
        expResult = 0;

        int result;
        result = instance.getPreferencias().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of setPreferenciasAlimenticias method, of class Usuario.
     */
    @Test
    public void testSetPreferenciasAlimenticiasNula() {
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias;
        preferencias = null;

        instance.setPreferenciasAlimenticias(preferencias);

        ArrayList<Repositorio.PreferenciasAlimenticias> result;
        result = instance.getPreferencias();

        assertNull(result);
    }

    /**
     * Test of setPreferenciasAlimenticias method, of class Usuario.
     */
    @Test
    public void testSetPreferenciasAlimenticiasaConUnElemento() {
        Repositorio.PreferenciasAlimenticias preferencia;
        preferencia = Repositorio.PreferenciasAlimenticias.Vegano;
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        instance.setPreferenciasAlimenticias(preferencias);

        Repositorio.PreferenciasAlimenticias expResult;
        expResult = Repositorio.PreferenciasAlimenticias.Vegano;

        Repositorio.PreferenciasAlimenticias result;
        result = instance.getPreferencias().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of setRestriccionesAlimenticias method, of class Usuario.
     */
    @Test
    public void testSetRestriccionesAlimenticiasVacia() {
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        instance.setRestriccionesAlimenticias(restricciones);

        int expResult;
        expResult = 0;

        int result;
        result = instance.getRestricciones().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of setRestriccionesAlimenticias method, of class Usuario.
     */
    @Test
    public void testSetRestriccionesAlimenticiasNula() {
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones;
        restricciones = null;

        instance.setRestriccionesAlimenticias(restricciones);

        ArrayList<Repositorio.RestriccionesAlimenticias> result;
        result = instance.getRestricciones();

        assertNull(result);
    }

    /**
     * Test of setRestriccionesAlimenticias method, of class Usuario.
     */
    @Test
    public void testSetRestriccionesAlimenticiasConUnElemento() {
        Repositorio.RestriccionesAlimenticias restriccion;
        restriccion = Repositorio.RestriccionesAlimenticias.Hipertenso;
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        instance.setRestriccionesAlimenticias(restricciones);

        Repositorio.RestriccionesAlimenticias expResult;
        expResult = Repositorio.RestriccionesAlimenticias.Hipertenso;

        Repositorio.RestriccionesAlimenticias result;
        result = instance.getRestricciones().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of getNacionalidad method, of class Usuario.
     */
    @Test
    public void testGetNacionalidadVacia() {
        instance.setNacionalidad("");

        String expResult;
        expResult = "";

        String result;
        result = instance.getNacionalidad();

        assertEquals(expResult, result);
    }

    /**
     * Test of getNacionalidad method, of class Usuario.
     */
    @Test
    public void testGetNacionalidadNoVacia() {
        instance.setNacionalidad("Uruguay");

        String expResult;
        expResult = "Uruguay";

        String result;
        result = instance.getNacionalidad();

        assertEquals(expResult, result);
    }

    /**
     * Test of getPreferencias method, of class Usuario.
     */
    @Test
    public void testGetPreferenciasVacia() {
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        instance.setPreferenciasAlimenticias(preferencias);

        int expResult;
        expResult = 0;

        int result;
        result = instance.getPreferencias().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of getPreferencias method, of class Usuario.
     */
    @Test
    public void testGetPreferenciasNula() {
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias;
        preferencias = null;

        instance.setPreferenciasAlimenticias(preferencias);

        ArrayList<Repositorio.PreferenciasAlimenticias> result;
        result = instance.getPreferencias();

        assertNull(result);
    }

    /**
     * Test of getPreferencias method, of class Usuario.
     */
    @Test
    public void testGetPreferenciasConUnElemento() {
        Repositorio.PreferenciasAlimenticias preferencia;
        preferencia = Repositorio.PreferenciasAlimenticias.Vegano;
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias;
        preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        instance.setPreferenciasAlimenticias(preferencias);

        Repositorio.PreferenciasAlimenticias expResult;
        expResult = Repositorio.PreferenciasAlimenticias.Vegano;

        Repositorio.PreferenciasAlimenticias result;
        result = instance.getPreferencias().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of getRestricciones method, of class Usuario.
     */
    @Test
    public void testGetRestriccionesVacia() {
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        instance.setRestriccionesAlimenticias(restricciones);

        int expResult;
        expResult = 0;

        int result;
        result = instance.getRestricciones().size();

        assertEquals(expResult, result);
    }

    /**
     * Test of getRestricciones method, of class Usuario.
     */
    @Test
    public void testGetRestriccionesNula() {
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones;
        restricciones = null;

        instance.setRestriccionesAlimenticias(restricciones);

        ArrayList<Repositorio.RestriccionesAlimenticias> result;
        result = instance.getRestricciones();

        assertNull(result);
    }

    /**
     * Test of getRestricciones method, of class Usuario.
     */
    @Test
    public void testGetRestriccionesConUnElemento() {
        Repositorio.RestriccionesAlimenticias restriccion;
        restriccion = Repositorio.RestriccionesAlimenticias.Hipertenso;
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones;
        restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        instance.setRestriccionesAlimenticias(restricciones);

        Repositorio.RestriccionesAlimenticias expResult;
        expResult = Repositorio.RestriccionesAlimenticias.Hipertenso;

        Repositorio.RestriccionesAlimenticias result;
        result = instance.getRestricciones().get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of getConsultas method, of class Usuario.
     */
    @Test
    public void testGetConsultasConUnElemento() {
        Consulta consulta = new Consulta();
        instance.agregarConsulta(consulta);

        ArrayList<Consulta> expResult;
        expResult = new ArrayList<>();
        expResult.add(consulta);

        ArrayList<Consulta> result = instance.getConsultas();

        assertEquals(expResult, result);
    }

    /**
     * Test of getPlanAlimentacionSemanal method, of class Usuario.
     */
    @Test
    public void testGetPlanAlimentacionSemanal() {
        Profesional profesional = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesional);
        instance.agregarPlanAlimentacionSemanal(plan);

        PlanAlimentacionSemanal expResult;
        expResult = plan;

        PlanAlimentacionSemanal result;
        result = instance.getPlanAlimentacionSemanalDe(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of getConsultaDe method, of class Usuario.
     */
    @Test(expected = AssertionError.class)
    public void testGetConsultaDeConNingunProfesionalConsultado() {
        Profesional profesional = new Profesional();

        Consulta result;
        result = instance.getConsultaDe(profesional);

        assertNotNull(result);
    }

    /**
     * Test of getConsultaDe method, of class Usuario.
     */
    @Test
    public void testGetConsultaDeConUnProfesionalConsultado() {
        Profesional profesional = new Profesional();
        Consulta consulta = new Consulta(profesional);
        instance.agregarConsulta(consulta);

        Consulta expResult;
        expResult = consulta;

        Consulta result;
        result = instance.getConsultaDe(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of getConsultaDe method, of class Usuario.
     */
    @Test
    public void testGetConsultaDeConDosProfesionalesConsultados() {
        Profesional profesionalUno = new Profesional();
        Consulta consultaUno = new Consulta(profesionalUno);
        instance.agregarConsulta(consultaUno);

        Profesional profesionalDos = new Profesional();
        Consulta consultaDos = new Consulta(profesionalDos);
        instance.agregarConsulta(consultaDos);

        Consulta expResult;
        expResult = consultaUno;

        Consulta result;
        result = instance.getConsultaDe(profesionalUno);

        assertEquals(expResult, result);
    }

    /**
     * Test of getPlanAlimentacionSemanalDe method, of class Usuario.
     */
    @Test(expected = AssertionError.class)
    public void testGetPlanAlimentacionSemanalSinProfesionales() {
        Profesional profesional = new Profesional();

        PlanAlimentacionSemanal result;
        result = instance.getPlanAlimentacionSemanalDe(profesional);
    }

    /**
     * Test of getPlanAlimentacionSemanalDe method, of class Usuario.
     */
    @Test
    public void testGetPlanAlimentacionSemanalDeUnProfesional() {
        Profesional profesional = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesional);
        instance.agregarPlanAlimentacionSemanal(plan);

        PlanAlimentacionSemanal expResult;
        expResult = plan;

        PlanAlimentacionSemanal result;
        result = instance.getPlanAlimentacionSemanalDe(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of getPlanAlimentacionSemanalDe method, of class Usuario.
     */
    @Test
    public void testGetPlanAlimentacionSemanalDeConDosProfesionales() {
        Profesional profesionalUno = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesionalUno);
        instance.agregarPlanAlimentacionSemanal(plan);

        Profesional profesionalDos = new Profesional();
        PlanAlimentacionSemanal planDos = new PlanAlimentacionSemanal(profesionalDos);
        instance.agregarPlanAlimentacionSemanal(planDos);

        PlanAlimentacionSemanal expResult;
        expResult = plan;

        PlanAlimentacionSemanal result;
        result = instance.getPlanAlimentacionSemanalDe(profesionalUno);

        assertEquals(expResult, result);
    }

    /**
     * Test of getIngestasDiarias method, of class Usuario.
     */
    @Test
    public void testGetIngestasDiariasConUnAlimento() {
        Alimento alimentoUno;
        alimentoUno = new Alimento();

        ArrayList<Alimento> ingestasDeHoy = new ArrayList<>();
        ingestasDeHoy.add(alimentoUno);
        instance.agregarIngestaDeHoy(ingestasDeHoy);

        Date hoy = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(hoy);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        hoy = calendar.getTime();

        Alimento expResult;
        expResult = alimentoUno;

        Hashtable<Date, ArrayList<Alimento>> table = instance.getIngestasDiarias();

        Alimento result = table.get(hoy).get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of agregarConsulta method, of class Usuario.
     */
    @Test
    public void testAgregarConsulta() {
        Profesional profesional = new Profesional();
        Consulta consulta = new Consulta(profesional);
        instance.agregarConsulta(consulta);

        Consulta expResult;
        expResult = consulta;

        Consulta result;
        result = instance.getConsultaDe(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of agregarIngestaDeHoy method, of class Usuario.
     */
    @Test
    public void testAgregarIngestaDeHoyConDosAlimentos() {
        Alimento alimentoUno;
        alimentoUno = new Alimento();

        Alimento alimentoDos;
        alimentoDos = new Alimento();

        ArrayList<Alimento> ingestasDeHoy = new ArrayList<>();
        ingestasDeHoy.add(alimentoUno);
        ingestasDeHoy.add(alimentoDos);
        instance.agregarIngestaDeHoy(ingestasDeHoy);

        Date hoy = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(hoy);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        hoy = calendar.getTime();

        Alimento expResultUno;
        expResultUno = alimentoUno;

        Alimento expResultDos;
        expResultDos = alimentoDos;

        Hashtable<Date, ArrayList<Alimento>> table = instance.getIngestasDiarias();

        Alimento resultUno = table.get(hoy).get(0);

        Alimento resultDos = table.get(hoy).get(1);

        assertTrue(expResultUno.equals(resultUno)
                && expResultDos.equals(resultDos));
    }

    /**
     * Test of agregarPlanAlimentacionSemanal method, of class Usuario.
     */
    @Test
    public void testAgregarPlanAlimentacionSemanal() {
        Profesional profesional = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesional);
        instance.agregarPlanAlimentacionSemanal(plan);

        PlanAlimentacionSemanal expResult;
        expResult = plan;

        PlanAlimentacionSemanal result;
        result = instance.getPlanAlimentacionSemanalDe(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of actualizarIngestaDeHoy method, of class Usuario.
     */
    @Test
    public void testActualizarIngestaDeHoy() {
        Alimento alimentoUno;
        alimentoUno = new Alimento();

        ArrayList<Alimento> ingestasDeHoy = new ArrayList<>();
        ingestasDeHoy.add(alimentoUno);
        instance.agregarIngestaDeHoy(ingestasDeHoy);

        Alimento alimentoNuevo;
        alimentoNuevo = new Alimento();
        ArrayList<Alimento> ingestasDeHoyNuevas = new ArrayList<>();
        ingestasDeHoyNuevas.add(alimentoNuevo);
        instance.actualizarIngestaDeHoy(ingestasDeHoyNuevas);

        Alimento expResult;
        expResult = alimentoNuevo;

        Date hoy = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(hoy);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        hoy = calendar.getTime();

        Alimento result;
        result = instance.getIngestasDiarias().get(hoy).get(0);

        assertEquals(expResult, result);
    }

    /**
     * Test of actualizarPlanAlimentacionSemanal method, of class Usuario.
     */
    @Test
    public void testActualizarPlanAlimentacionSemanal() {
        Profesional profesional = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesional);
        instance.agregarPlanAlimentacionSemanal(plan);

        PlanAlimentacionSemanal planNuevo = new PlanAlimentacionSemanal(profesional);
        instance.actualizarPlanAlimentacionSemanal(planNuevo);

        PlanAlimentacionSemanal expResult;
        expResult = planNuevo;

        PlanAlimentacionSemanal result;
        result = instance.getPlanAlimentacionSemanalDe(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of existeConsulta method, of class Usuario.
     */
    @Test
    public void testExisteConsultaConConsultaExistente() {
        Profesional profesional = new Profesional();
        Consulta consulta = new Consulta(profesional);
        instance.agregarConsulta(consulta);

        boolean expResult;
        expResult = true;

        boolean result;
        result = instance.existeConsulta(profesional);

        assertEquals(expResult, result);
    }

    /**
     * Test of existeConsulta method, of class Usuario.
     */
    @Test
    public void testExisteConsultaSinConsultaExistente() {
        Profesional profesionalSinConsultas = new Profesional();
        profesionalSinConsultas.setId(0);

        Profesional profesional = new Profesional();
        profesional.setId(1);
        Consulta consulta = new Consulta(profesional);
        instance.agregarConsulta(consulta);

        boolean expResult;
        expResult = false;

        boolean result;
        result = instance.existeConsulta(profesionalSinConsultas);

        assertEquals(expResult, result);
    }

    /**
     * Test of existeIngestaDeHoy method, of class Usuario.
     */
    @Test
    public void testExisteIngestaDeHoyConIngestaNoExistente() {
        boolean expResult;
        expResult = false;

        boolean result;
        result = instance.existeIngestaDeHoy();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeIngestaDeHoy method, of class Usuario.
     */
    @Test
    public void testExisteIngestaDeHoyConIngestaExistente() {
        Alimento alimentoUno;
        alimentoUno = new Alimento();

        ArrayList<Alimento> ingestasDeHoy = new ArrayList<>();
        ingestasDeHoy.add(alimentoUno);
        instance.agregarIngestaDeHoy(ingestasDeHoy);

        boolean expResult;
        expResult = true;

        boolean result;
        result = instance.existeIngestaDeHoy();

        assertEquals(expResult, result);
    }

    /**
     * Test of existePlanAlimentacionSemanal method, of class Usuario.
     */
    @Test
    public void testExistePlanAlimentacionSemanalConPlanExistente() {
        Profesional profesional = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesional);
        instance.agregarPlanAlimentacionSemanal(plan);

        boolean expResult;
        expResult = true;

        boolean result;
        result = instance.existePlanAlimentacionSemanal(plan);

        assertEquals(expResult, result);
    }

    /**
     * Test of existePlanAlimentacionSemanal method, of class Usuario.
     */
    @Test
    public void testExistePlanAlimentacionSemanalConPlanNoExistente() {
        Profesional profesional = new Profesional();
        PlanAlimentacionSemanal plan = new PlanAlimentacionSemanal(profesional);

        boolean expResult;
        expResult = false;

        boolean result;
        result = instance.existePlanAlimentacionSemanal(plan);

        assertEquals(expResult, result);
    }

}
