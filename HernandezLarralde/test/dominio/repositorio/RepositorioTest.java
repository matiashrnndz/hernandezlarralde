/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.repositorio;

import dominio.alimentos.Alimento;
import dominio.consulta.Consulta;
import dominio.personas.Profesional;
import dominio.personas.Usuario;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class RepositorioTest {
    
    Repositorio instance;
    
    public RepositorioTest() {
    }
    
    @Before
    public void setUp() {
        instance = new Repositorio();
    }

    /**
     * Test of getAlimentoAlAzar method, of class Repositorio.
     */
    @Test
    public void testGetAlimentoAlAzarSinElementos() {
        Alimento alimento = new Alimento();
        
        String expResult = alimento.getNombre();
        
        String result = instance.getAlimentoAlAzar().getNombre();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAlimentoAlAzar method, of class Repositorio.
     */
    @Test
    public void testGetAlimentoAlAzarConUnElemento() {
        Alimento arroz = new Alimento();
        arroz.setNombre("arroz");
        instance.agregarAlimento(arroz);
        
        Alimento expResult;
        expResult = arroz;
        
        Alimento result;
        result = instance.getAlimentoAlAzar();
        
        assertEquals(expResult, result);
    }
    

    /**
     * Test of getIdProfesional method, of class Repositorio.
     */
    @Test
    public void testGetIdProfesionalPrimerProfesional() {
        Profesional profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        int expResult = 1;
        
        int result = instance.getIdProfesional();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getIdProfesional method, of class Repositorio.
     */
    @Test
    public void testGetIdProfesionalSegundoProfesional() {
        Profesional profesional;
        profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        Profesional profesionalDos;
        profesionalDos = new Profesional();
        instance.agregarProfesional(profesionalDos);
        
        int expResult = 2;
        
        int result;
        result = instance.getIdProfesional();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getProfesionales method, of class Repositorio.
     */
    @Test
    public void testGetProfesionalesVacio() {
        int expResult;
        expResult = 0;
        
        int result;
        result = instance.getProfesionales().size();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getProfesionales method, of class Repositorio.
     */
    @Test
    public void testGetProfesionalesConUnElemento() {
        Profesional profesional = new Profesional();
        ArrayList<Profesional> profesionales;
        profesionales = new ArrayList<>();
        profesionales.add(profesional);
        instance.agregarProfesional(profesional);
        
        Profesional expResult;
        expResult = profesionales.get(0);
        
        Profesional result;
        result = instance.getProfesionales().get(0);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getUsuarios method, of class Repositorio.
     */
    @Test
    public void testGetUsuariosVacio() {
        int expResult;
        expResult = 0;
        
        int result;
        result = instance.getUsuarios().size();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getUsuarios method, of class Repositorio.
     */
    @Test
    public void testGetUsuariosConUnElemento() {
        Usuario usuario = new Usuario();
        ArrayList<Usuario> usuarios;
        usuarios = new ArrayList<>();
        usuarios.add(usuario);
        instance.agregarUsuario(usuario);
        
        Usuario expResult;
        expResult = usuarios.get(0);
        
        Usuario result;
        result = instance.getUsuarios().get(0);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlimentos method, of class Repositorio.
     */
    @Test
    public void testGetAlimentosVacio() {
        int expResult;
        expResult = 0;
        
        int result;
        result = instance.getProfesionales().size();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAlimentos method, of class Repositorio.
     */
    @Test
    public void testGetAlimentosConUnElemento() {
        Alimento alimento = new Alimento();
        ArrayList<Alimento> alimentos;
        alimentos = new ArrayList<>();
        alimentos.add(alimento);
        instance.agregarAlimento(alimento);
        
        Alimento expResult;
        expResult = alimentos.get(0);
        
        Alimento result;
        result = instance.getAlimentos().get(0);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getUsuariosConConsulta method, of class Repositorio.
     */
    @Test
    public void testGetUsuariosConConsultaSinUsuarios() {
        Profesional profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        int expResult = 0;
        
        int result = instance.getUsuariosConConsulta(profesional).size();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getUsuariosConConsulta method, of class Repositorio.
     */
    @Test
    public void testGetUsuariosConConsultaUnUsuarioSinConsulta() {
        Profesional profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        Usuario usuario = new Usuario();
        instance.agregarUsuario(usuario);
        
        int expResult = 0;
        
        int result = instance.getUsuariosConConsulta(profesional).size();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getUsuariosConConsulta method, of class Repositorio.
     */
    @Test
    public void testGetUsuariosConConsultaUnUsuarioSinConsultaYUnoConConsulta() {
        Profesional profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        Usuario usuario = new Usuario();
        Consulta consulta = new Consulta(profesional);
        consulta.consultar("Consulta");
        usuario.agregarConsulta(consulta);
        instance.agregarUsuario(usuario);
        
        Usuario usuarioDos = new Usuario();
        instance.agregarUsuario(usuarioDos);
        
        int expResult = 1;
        
        int result = instance.getUsuariosConConsulta(profesional).size();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getUsuariosConConsulta method, of class Repositorio.
     */
    @Test
    public void testGetUsuariosConConsultaUnUsuarioConConsulta() {
        Profesional profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        Usuario usuario = new Usuario();
        Consulta consulta = new Consulta(profesional);
        consulta.consultar("Consulta");
        usuario.agregarConsulta(consulta);
        instance.agregarUsuario(usuario);

        Usuario expResult = usuario;
        
        Usuario result = instance.getUsuariosConConsulta(profesional).get(0);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of agregarProfesional method, of class Repositorio.
     */
    @Test
    public void testAgregarProfesionalNulo() {
        Profesional profesional = null;
        instance.agregarProfesional(profesional);
        
        boolean expResult = true;
        
        boolean result = instance.getProfesionales().isEmpty();
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of agregarProfesional method, of class Repositorio.
     */
    @Test
    public void testAgregarProfesionalPrimero() {
        Profesional profesional = new Profesional();
        instance.agregarProfesional(profesional);
        
        Profesional expResult = profesional;
        
        Profesional result = instance.getProfesionales().get(0);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarUsuario method, of class Repositorio.
     */
    @Test
    public void testAgregarUsuarioNulo() {
        Usuario usuario = null;
        instance.agregarUsuario(usuario);
        
        boolean expResult = true;
        
        boolean result = instance.getUsuarios().isEmpty();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of agregarUsuario method, of class Repositorio.
     */
    @Test
    public void testAgregarUsuarioPrimero() {
        Usuario usuario = new Usuario();
        
        instance.agregarUsuario(usuario);
        
        Usuario expResult = usuario;
        
        Usuario result = instance.getUsuarios().get(0);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarAlimento method, of class Repositorio.
     */
    @Test
    public void testAgregarAlimentoNulo() {
        Alimento alimento = null;
        instance.agregarAlimento(alimento);
        
        boolean expResult = true;
        
        boolean result = instance.getAlimentos().isEmpty();
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of agregarAlimento method, of class Repositorio.
     */
    @Test
    public void testAgregarAlimentoPrimero() {
        Alimento alimento = new Alimento();
        instance.agregarAlimento(alimento);
        
        Alimento expResult = alimento;
        
        Alimento result = instance.getAlimentos().get(0);
        
        assertEquals(expResult, result);
    }
    
}
