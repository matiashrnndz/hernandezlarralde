/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorConsultaTest {
    
    ValidadorConsulta instance;
    
    public ValidadorConsultaTest() {
    }
    
    @Before
    public void setUp() {
        instance = new ValidadorConsulta();
    }

    /**
     * Test of validarConsultaNoVacia method, of class ValidadorConsulta.
     */
    @Test
    public void testValidarConsultaNoVaciaStringVacio() {
        String consulta = "";
        
        boolean expResult = false;
        
        boolean result = instance.validarConsultaNoVacia(consulta);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarConsultaNoVacia method, of class ValidadorConsulta.
     */
    @Test
    public void testValidarConsultaNoVaciaStringNoVacio() {
        String consulta = "Consulta";
        
        boolean expResult = true;
        
        boolean result = instance.validarConsultaNoVacia(consulta);
        
        assertEquals(expResult, result);
    }
    
}
