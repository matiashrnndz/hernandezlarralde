/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorPersonaTest {
    
    ValidadorPersona instance;
    
    public ValidadorPersonaTest() {
    }
    
    @Before
    public void setUp() {
        instance = new ValidadorPersona();
    }

    /**
     * Test of validarApellido method, of class ValidadorPersona.
     */
    @Test
    public void testValidarApellidoVacio() {
        String apellido = "";
        
        boolean expResult = false;
        
        boolean result = instance.validarApellido(apellido);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarApellido method, of class ValidadorPersona.
     */
    @Test
    public void testValidarApellidoConNumero() {
        String apellido = "Jorg3";
        
        boolean expResult = false;
        
        boolean result = instance.validarApellido(apellido);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarApellido method, of class ValidadorPersona.
     */
    @Test
    public void testValidarApellidoCorrecta() {
        String apellido = "Jorge";
        
        boolean expResult = true;
        
        boolean result = instance.validarApellido(apellido);
        
        assertEquals(expResult, result);
    }
    

    /**
     * Test of validarNacionalidad method, of class ValidadorPersona.
     */
    @Test
    public void testValidarNacionalidadVacia() {
        String nacionalidad = "";
        
        boolean expResult = true;
        
        boolean result = instance.validarNacionalidad(nacionalidad);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarNacionalidad method, of class ValidadorPersona.
     */
    @Test
    public void testValidarNacionalidadConNumero() {
        String nacionalidad = "Urugu4y";
        
        boolean expResult = false;
        
        boolean result = instance.validarNacionalidad(nacionalidad);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarNacionalidad method, of class ValidadorPersona.
     */
    @Test
    public void testValidarNacionalidadCorrecta() {
        String nacionalidad = "Uruguay";
        
        boolean expResult = true;
        
        boolean result = instance.validarNacionalidad(nacionalidad);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of hayApellido method, of class ValidadorPersona.
     */
    @Test
    public void testHayApellidoVacio() {
        String apellido = "";
        
        boolean expResult = false;
        
        boolean result = instance.hayApellido(apellido);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayApellido method, of class ValidadorPersona.
     */
    @Test
    public void testHayApellidoStringApellido() {
        String apellido = "Apellido";
        
        boolean expResult = false;
        
        boolean result = instance.hayApellido(apellido);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayApellido method, of class ValidadorPersona.
     */
    @Test
    public void testHayApellidoStringApellidoCorrecto() {
        String apellido = "Larralde";
        
        boolean expResult = true;
        
        boolean result = instance.hayApellido(apellido);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayNacionalidad method, of class ValidadorPersona.
     */
    @Test
    public void testHayNacionalidadVacio() {
        String nacionalidad = "Vacio";
        
        boolean expResult = true;
        
        boolean result = instance.hayNacionalidad(nacionalidad);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayNacionalidad method, of class ValidadorPersona.
     */
    @Test
    public void testHayNacionalidadStringNacionalidad() {
        String nacionalidad = "Nacionalidad";
        
        boolean expResult = false;
        
        boolean result = instance.hayNacionalidad(nacionalidad);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayNacionalidad method, of class ValidadorPersona.
     */
    @Test
    public void testHayNacionalidadCorrecto() {
        String nacionalidad = "Uruguay";
        
        boolean expResult = true;
        
        boolean result = instance.hayNacionalidad(nacionalidad);
        
        assertEquals(expResult, result);
    }
    
}
