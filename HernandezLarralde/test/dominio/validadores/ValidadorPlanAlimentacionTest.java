/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import dominio.alimentos.Alimento;
import dominio.alimentos.Nutriente;
import dominio.personas.Usuario;
import dominio.repositorio.Repositorio;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorPlanAlimentacionTest {

    private ValidadorPlanAlimentacion instance;
    private Repositorio repositorio;
    private Usuario usuario;

    public ValidadorPlanAlimentacionTest() {
    }

    @Before
    public void setUp() {
        repositorio = new Repositorio();
        usuario = new Usuario();
        instance = new ValidadorPlanAlimentacion(usuario, repositorio);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnAlimentoValido() {
        Alimento alimento = new Alimento();
        alimento.setNombre("Arroz");
        repositorio.agregarAlimento(alimento);

        boolean expResult = true;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnRestringido() {
        Repositorio.RestriccionesAlimenticias restriccion = Repositorio.RestriccionesAlimenticias.Hipertenso;
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        usuario.setRestriccionesAlimenticias(restricciones);

        Alimento arroz = new Alimento();
        Nutriente.Nutrientes sodio = Nutriente.Nutrientes.Sodio;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(sodio);
        nutriente.setValor(2);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        arroz.setNutrientes(nutrientes);
        arroz.setNombre("Arroz");
        repositorio.agregarAlimento(arroz);

        boolean expResult = false;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnoNoPreferente() {
        Repositorio.PreferenciasAlimenticias preferencia = Repositorio.PreferenciasAlimenticias.Vegetariano;
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        usuario.setPreferenciasAlimenticias(preferencias);

        Alimento milanesa = new Alimento();
        Alimento.TipoAlimento carne = Alimento.TipoAlimento.Carne;
        ArrayList<Alimento.TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(carne);
        milanesa.setTipoAlimento(tiposAlimento);
        milanesa.setNombre("Milanesa");
        repositorio.agregarAlimento(milanesa);

        boolean expResult = false;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnoNoPreferenteYUnoValido() {
        Repositorio.PreferenciasAlimenticias preferencia = Repositorio.PreferenciasAlimenticias.Ovolacteovegetariano;
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        usuario.setPreferenciasAlimenticias(preferencias);

        Alimento milanesa = new Alimento();
        Alimento.TipoAlimento carne = Alimento.TipoAlimento.Carne;
        ArrayList<Alimento.TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(carne);
        milanesa.setTipoAlimento(tiposAlimento);
        milanesa.setNombre("Milanesa");
        repositorio.agregarAlimento(milanesa);

        Alimento bebida = new Alimento();
        bebida.setNombre("CocaCola");
        repositorio.agregarAlimento(bebida);

        boolean expResult = true;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnRestringidoYUnoValido() {
        Repositorio.RestriccionesAlimenticias restriccion = Repositorio.RestriccionesAlimenticias.Hipertenso;
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        usuario.setRestriccionesAlimenticias(restricciones);

        Alimento arroz = new Alimento();
        Nutriente.Nutrientes sodio = Nutriente.Nutrientes.Sodio;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(sodio);
        nutriente.setValor(2);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        arroz.setNutrientes(nutrientes);
        arroz.setNombre("Arroz");
        repositorio.agregarAlimento(arroz);

        Alimento bebida = new Alimento();
        bebida.setNombre("CocaCola");
        repositorio.agregarAlimento(bebida);

        boolean expResult = true;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnRestringidoYUnoNoPreferente() {
        Repositorio.RestriccionesAlimenticias restriccion = Repositorio.RestriccionesAlimenticias.Diabético;
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        usuario.setRestriccionesAlimenticias(restricciones);

        Repositorio.PreferenciasAlimenticias preferencia = Repositorio.PreferenciasAlimenticias.Vegano;
        
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        usuario.setPreferenciasAlimenticias(preferencias);

        Alimento cocaCola = new Alimento();
        Nutriente.Nutrientes sodio = Nutriente.Nutrientes.Azúcares;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(sodio);
        nutriente.setValor(2);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        cocaCola.setNutrientes(nutrientes);
        cocaCola.setNombre("CocaCola");
        repositorio.agregarAlimento(cocaCola);

        Alimento milanesa = new Alimento();
        Alimento.TipoAlimento carne = Alimento.TipoAlimento.Carne;
        ArrayList<Alimento.TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(carne);
        milanesa.setTipoAlimento(tiposAlimento);
        milanesa.setNombre("Milanesa");
        repositorio.agregarAlimento(milanesa);

        boolean expResult = false;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }

    /**
     * Test of existeAlgunAlimentoValido method, of class
     * ValidadorPlanAlimentacion.
     */
    @Test
    public void testExisteAlgunAlimentoValidoConUnRestringidoYUnoNoPreferenteYUnoValido() {
        Repositorio.RestriccionesAlimenticias restriccion = Repositorio.RestriccionesAlimenticias.Hipertenso;
        ArrayList<Repositorio.RestriccionesAlimenticias> restricciones = new ArrayList<>();
        restricciones.add(restriccion);
        usuario.setRestriccionesAlimenticias(restricciones);

        Repositorio.PreferenciasAlimenticias preferencia = Repositorio.PreferenciasAlimenticias.Vegetariano;
        ArrayList<Repositorio.PreferenciasAlimenticias> preferencias = new ArrayList<>();
        preferencias.add(preferencia);
        usuario.setPreferenciasAlimenticias(preferencias);

        Alimento arroz = new Alimento();
        Nutriente.Nutrientes sodio = Nutriente.Nutrientes.Sodio;
        Nutriente nutriente = new Nutriente();
        nutriente.setNombre(sodio);
        nutriente.setValor(2);
        ArrayList<Nutriente> nutrientes = new ArrayList<>();
        nutrientes.add(nutriente);
        arroz.setNutrientes(nutrientes);
        arroz.setNombre("Arroz");
        repositorio.agregarAlimento(arroz);

        Alimento milanesa = new Alimento();
        Alimento.TipoAlimento carne = Alimento.TipoAlimento.Carne;
        ArrayList<Alimento.TipoAlimento> tiposAlimento = new ArrayList<>();
        tiposAlimento.add(carne);
        milanesa.setTipoAlimento(tiposAlimento);
        milanesa.setNombre("Milanesa");
        repositorio.agregarAlimento(milanesa);
        
        Alimento bebida = new Alimento();
        bebida.setNombre("CocaCola");
        repositorio.agregarAlimento(bebida);

        boolean expResult = true;

        boolean result = instance.existeAlgunAlimentoValido();

        assertEquals(expResult, result);
    }
    
}
