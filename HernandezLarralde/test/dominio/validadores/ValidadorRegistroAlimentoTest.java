/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorRegistroAlimentoTest {
    
    private ValidadorRegistroAlimento instance;
    
    public ValidadorRegistroAlimentoTest() {
    }
    
    @Before
    public void setUp() {
        instance = new ValidadorRegistroAlimento();
    }

    /**
     * Test of validarValorNutriente method, of class ValidadorRegistroAlimento.
     */
    @Test
    public void testValidarValorNutrienteCorrecto() {
        int valorActual = 5;
        
        boolean expResult = true;
        
        boolean result = instance.validarValorNutriente(valorActual);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarValorNutriente method, of class ValidadorRegistroAlimento.
     */
    @Test
    public void testValidarValorNutrienteNegativo() {
        int valorActual = -1;
        
        boolean expResult = false;
        
        boolean result = instance.validarValorNutriente(valorActual);
        
        assertEquals(expResult, result);
    }

     /**
     * Test of validarValorNutriente method, of class ValidadorRegistroAlimento.
     */
    @Test
    public void testValidarValorNutrienteCero() {
        int valorActual = 0;
        
        boolean expResult = true;
        
        boolean result = instance.validarValorNutriente(valorActual);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarPertenenciaTipoAlimento method, of class ValidadorRegistroAlimento.
     */
    @Test
    public void testValidarPertenenciaTipoAlimentoPetenece() {
        boolean pertenece = true;
        
        boolean expResult = true;
        
        boolean result = instance.validarPertenenciaTipoAlimento(pertenece);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarPertenenciaTipoAlimento method, of class ValidadorRegistroAlimento.
     */
    @Test
    public void testValidarPertenenciaTipoAlimentoNoPertenece() {
        boolean pertenece = false;
        
        boolean expResult = false;
        
        boolean result = instance.validarPertenenciaTipoAlimento(pertenece);
        
        assertEquals(expResult, result);
    }
    
}
