/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorRegistroProfesionalTest {
    
    private ValidadorRegistroProfesional instance;
    
    public ValidadorRegistroProfesionalTest() {
    }
    
    @Before
    public void setUp() {
        instance = new ValidadorRegistroProfesional();
    }

    /**
     * Test of validarPaisTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testValidarPaisTituloVacio() {
        String paisTitulo = "";
        
        boolean expResult = false;
        
        boolean result = instance.validarPaisTitulo(paisTitulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarPaisTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testValidarPaisTituloCorrecto() {
        String paisTitulo = "Uruguay";
        
        boolean expResult = true;
        
        boolean result = instance.validarPaisTitulo(paisTitulo);
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of validarPaisTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testValidarPaisTituloConNumero() {
        String paisTitulo = "Urugu4y";
        
        boolean expResult = false;
        
        boolean result = instance.validarPaisTitulo(paisTitulo);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of hayPaisTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testHayPaisTituloCorrecto() {
        String paisTitulo = "Uruguay";
        
        boolean expResult = true;
        
        boolean result = instance.hayPaisTitulo(paisTitulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayPaisTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testHayPaisTituloVacio() {
        String paisTitulo = "";
        
        boolean expResult = false;
        
        boolean result = instance.hayPaisTitulo(paisTitulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayPaisTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testHayPaisTituloStringPaisDelTitulo() {
        String paisTitulo = "País del título :";
        
        boolean expResult = false;
        
        boolean result = instance.hayPaisTitulo(paisTitulo);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of hayTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testHayTituloCorrecto() {
        String titulo = "Nutricionista";
        
        boolean expResult = true;
        
        boolean result = instance.hayTitulo(titulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testHayTituloVacio() {
        String titulo = "";
        
        boolean expResult = false;
        
        boolean result = instance.hayTitulo(titulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testHayTituloStringTitulo() {
        String titulo = "Título";
        
        boolean expResult = false;
        
        boolean result = instance.hayTitulo(titulo);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of validarTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testValidarTituloVacio() {
        String titulo = "";
        
        boolean expResult = false;
        
        boolean result = instance.validarTitulo(titulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testValidarTituloConNumero() {
        String titulo = "Nutricionist4";
        
        boolean expResult = false;
        
        boolean result = instance.validarTitulo(titulo);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarTitulo method, of class ValidadorRegistroProfesional.
     */
    @Test
    public void testValidarTituloCorrecto() {
        String titulo = "Nutricionista";
        
        boolean expResult = true;
        
        boolean result = instance.validarTitulo(titulo);
        
        assertEquals(expResult, result);
    }
    
}
