/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio.validadores;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matiashrnndz
 */
public class ValidadorTest {
    
    private ValidadorImpl instance;
    
    public ValidadorTest() {
    }
    
    @Before
    public void setUp() {
        instance = new ValidadorImpl();
    }

    /**
     * Test of validarNombre method, of class Validador.
     */
    @Test
    public void testValidarNombreCorrecto() {
        String nombre = "Nombre";
        
        boolean expResult = true;
        
        boolean result;
        result = instance.validarNombre(nombre);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of validarNombre method, of class Validador.
     */
    @Test
    public void testValidarNombreConNumero() {
        String nombre = "Nombre5";
        
        boolean expResult = false;
        
        boolean result;
        result = instance.validarNombre(nombre);
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of validarNombre method, of class Validador.
     */
    @Test
    public void testValidarNombreVacio() {
        String nombre = "";
        
        boolean expResult = true;
        
        boolean result;
        result = instance.validarNombre(nombre);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of hayNombre method, of class Validador.
     */
    @Test
    public void testHayNombreVacio() {
        String nombre = "";
        
        boolean expResult = false;
        
        boolean result = instance.hayNombre(nombre);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayNombre method, of class Validador.
     */
    @Test
    public void testHayNombreNombre() {
        String nombre = "Nombre";
        
        boolean expResult = false;
        
        boolean result = instance.hayNombre(nombre);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hayNombre method, of class Validador.
     */
    @Test
    public void testHayNombreCorrecto() {
        String nombre = "Test";
        
        boolean expResult = true;
        
        boolean result = instance.hayNombre(nombre);
        
        assertEquals(expResult, result);
    }

    public class ValidadorImpl extends Validador {

    }
    
}
